# Backend

Everything that runs on the backend of ZoneONE - importer, api, etc

---

## Projects

- `ZoneOne.Static.Importer` - Cron job to import the static GTFS data
- `ZoneOne.Static.Api` - REST API for static data
- `ZoneOne.Static.Data` - Static data structures
- `ZoneOne.Static.Database` - Extensions of `ZoneOne.Static.Data` to link it to the database
- `ZoneOne.Realtime` - Realtime GTFS data handler
- `ZoneOne.Realtime.Api` - WebSocket API for realtime data
- `ZoneOne.Backend` - Process that runs the other projects
