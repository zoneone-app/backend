using System.Collections.Generic;

namespace ZoneOne.Utilities
{
    public static class SimplifyUtil
    {
        public static KeyValuePair<TKey, TValue> ToKeyValuePair<TKey, TValue>(this (TKey, TValue) tuple) => 
            new(tuple.Item1, tuple.Item2);
    }
}