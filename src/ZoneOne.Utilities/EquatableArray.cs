using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace ZoneOne.Utilities
{
    public static class EquatableArrayUtil
    {
        public static EquatableArray<T> ToEquatable<T>(this IEnumerable<T> array) => new(array.ToArray());
    }

    public class EquatableArray<T> : IEnumerable<T>, IEquatable<EquatableArray<T>>
    {
        private readonly T[] _array;

        public EquatableArray(T[] array)
        {
            _array = array;
        }

        public IEnumerator<T> GetEnumerator()
        {
            return ((IEnumerable<T>) _array).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _array.GetEnumerator();
        }

        public bool Equals(EquatableArray<T>? other)
        {
            //Console.WriteLine("Checking for equality, other null? {0}, equal?: {1}", other == null, this.SequenceEqual(other));
            //return other != null && _array.Zip(other).All(tuple => object.Equals(tuple.First, tuple.Second));
            if (other == null) return false;
            var equal = this.SequenceEqual(other);
            return equal;
        }

        public override int GetHashCode()
        {
            return _array.Aggregate(0, (a, b) => a ^ b?.GetHashCode() ?? 0);
        }

        public override bool Equals(object? obj)
        {
            if (obj == null) return false;
            if (obj == this) return true;
            if (obj is EquatableArray<T> arr) return Equals(arr);
            return false;
        }
    }
}