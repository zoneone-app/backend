using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ZoneOne.Utilities
{
    public static class EnumerableUtil
    {
        public static TA GetMax<TA, TU>(this IEnumerable<TA> data, Func<TA, TU> f) where TU : IComparable
        {
            return data.Aggregate((i1, i2) => f(i1).CompareTo(f(i2)) > 0 ? i1 : i2);
        }

        // from https://gist.github.com/ldub/48a4e8bf02b03b4e91b1#file-unzip-linq-cs
        public static (IEnumerable<TA>, IEnumerable<TB>) Unzip<TA, TB>(this IEnumerable<(TA, TB)> seq)
        {
            var valueTuples = seq as (TA, TB)[] ?? seq.ToArray();
            return (
                valueTuples.Select(v => v.Item1),
                valueTuples.Select(v => v.Item2)
            );
        }
        
        public static (IEnumerable<TA>, IEnumerable<TB>, IEnumerable<TC>) Unzip<TA, TB, TC>(this IEnumerable<(TA, TB, TC)> seq)
        {
            var valueTuples = seq as (TA, TB, TC)[] ?? seq.ToArray();
            return (
                valueTuples.Select(v => v.Item1),
                valueTuples.Select(v => v.Item2),
                valueTuples.Select(v => v.Item3)
            );
        }

        public static async Task<(IEnumerable<TA>, IEnumerable<TB>)> 
            UnzipAsync<TA, TB>(this IAsyncEnumerable<(TA, TB)> seq)
        {
            var valueTuples = seq as (TA, TB)[] ?? await seq.ToArrayAsync();
            return (
                valueTuples.Select(v => v.Item1),
                valueTuples.Select(v => v.Item2)
            );
        }
        
        public static async Task<(IEnumerable<TA>, IEnumerable<TB>, IEnumerable<TC>)> 
            UnzipAsync<TA, TB, TC>(this IAsyncEnumerable<(TA, TB, TC)> seq)
        {
            var valueTuples = seq as (TA, TB, TC)[] ?? await seq.ToArrayAsync();
            return (
                valueTuples.Select(v => v.Item1),
                valueTuples.Select(v => v.Item2),
                valueTuples.Select(v => v.Item3)
            );
        }

        public static (IEnumerable<TSelect> select, TAggregate aggregate) AggregateSelect<T, TSelect, TAggregate>(
            this IEnumerable<T> seq, Func<T, TAggregate, (TSelect, TAggregate)> map, TAggregate start = default)
        {
            var source = seq.ToArray();
            
            var aggregate = start;
            var select = new TSelect[source.Length];

            for (var i = 0; i < source.Length; i++)
            {
                var (selectValue, newAggregate) = map(source[i], aggregate);
                
                aggregate = newAggregate;
                select[i] = selectValue;
            }

            return (select, aggregate);
        }

        public static async Task<(IEnumerable<TSelect> select, TAggregate aggregate)> AggregateSelect<T, TSelect, TAggregate>(
            this IAsyncEnumerable<T> seq, Func<T, TAggregate, (TSelect, TAggregate)> map, TAggregate start = default)
        {
            var source = await seq.ToArrayAsync();

            var aggregate = start;
            var select = new TSelect[source.Length];

            for (var i = 0; i < source.Length; i++)
            {
                var (selectValue, newAggregate) = map(source[i], aggregate);

                aggregate = newAggregate;
                select[i] = selectValue;
            }

            return (select, aggregate);
        }
        
        public static (int, int) CountMany<T>(this IEnumerable<T> seq, Predicate<T> a, Predicate<T> b)
        {
            var countA = 0;
            var countB = 0;
            
            foreach (var el in seq)
            {
                if (a(el)) countA++;
                if (b(el)) countB++;
            }

            return (countA, countB);
        }
        
        public static IEnumerable<(T, T)> Pair<T>(this IEnumerable<T> source)
        {
            var previous = default(T);
            var isFirst = true;
            
            foreach (var item in source)
            {
                var prev = previous;
                var wasFirst = isFirst;
                
                previous = item;
                isFirst = false;
                
                if (!wasFirst) yield return (prev, item);
            }
        }
        
        /// <summary>
        /// Groups each item with the previous item
        /// </summary>
        /// <remarks>Result has N-1 items, as the first is skipped</remarks>
        /// <returns></returns>
        public static async IAsyncEnumerable<(T, T)> Pair<T>(this IAsyncEnumerable<T> source)
        {
            var previous = default(T);
            var isFirst = true;
            
            await foreach (var item in source)
            {
                var prev = previous;
                var wasFirst = isFirst;
                
                previous = item;
                isFirst = false;
                
                if (!wasFirst) yield return (prev, item);
            }
        }

        #nullable enable
        public static IEnumerable<T> WhereNotNull<T>(this IEnumerable<T?> source) => source.Where(v => v != null)!;
        #nullable disable
    }
}