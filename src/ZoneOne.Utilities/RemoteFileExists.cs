using System;
using System.Diagnostics;
using System.Net;
using System.Threading.Tasks;

namespace ZoneOne.Utilities
{
    public static class RemoteFileExists
    {
        /// <summary>
        /// This method will check a url to see that it does not return server or protocol errors
        /// </summary>
        /// <remarks>
        /// https://stackoverflow.com/a/7180033
        /// </remarks>
        /// <param name="url">The path to check</param>
        /// <returns></returns>
        public static async Task<bool> UrlIsValidAsync(string url)
        {
            try
            {
                var request = (HttpWebRequest) WebRequest.Create(url);
                request.Timeout = 5000;
                request.Method = "HEAD";

                using var response = (HttpWebResponse) await request.GetResponseAsync();
                var statusCode = (int)response.StatusCode;
                if (statusCode is >= 100 and < 400)
                {
                    return true;
                }
            }
            catch (WebException ex)
            {
                if (ex.Status == WebExceptionStatus.ProtocolError)
                {
                    return false;
                }
            }
            return false;
        }
    }
}
