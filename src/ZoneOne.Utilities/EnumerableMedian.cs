using System;
using System.Collections.Generic;
using System.Linq;

namespace ZoneOne.Utilities
{
    /// <remarks>From https://stackoverflow.com/a/22702269</remarks>
    public static class EnumerableMedian
    {
        /// <summary>
        /// Swaps the items at index <see cref="i"/> and <see cref="j"/> with each other
        /// </summary>
        private static void Swap<T>(this IList<T> list, int i, int j)
        {
            if (i == j) return;
            (list[i], list[j]) = (list[j], list[i]);
        }
        
        /// <summary>
        /// Partitions the given list around a pivot element such that all elements on left of pivot are &#8804; pivot
        /// and the ones at the right are > pivot. This method can be used for sorting, N-order statistics such as
        /// as median finding algorithms.
        /// Reference: Introduction to Algorithms 3rd Edition, Corman et al, pp 171
        /// <remarks>
        /// Pivot is selected randomly if random number generator is supplied else its selected as last element in the list.
        /// </remarks>
        /// </summary>
        /// <returns>The index that was pivoted around</returns>
        private static int Partition<T>(this IList<T> source, int start, int end, Random random = null)
            where T : IComparable<T>
        {
            if (random != null) source.Swap(end, random.Next(start, end + 1));

            var pivot = source[end];
            var lastLow = start - 1;

            for (var i = start; i < end; i++)
                if (source[i].CompareTo(pivot) <= 0)
                    source.Swap(i, ++lastLow);

            source.Swap(end, ++lastLow);

            return lastLow;
        }

        /// <summary>
        /// Returns the nth smallest item from the list.
        /// Reference: Introduction to Algorithms 3rd Edition, Corman et al, pp 216
        /// </summary>
        /// <remarks>Source list is mutated</remarks>
        private static T NthOrderStatistic<T>(this IList<T> source, int n, int start, int end, Random random)
            where T : IComparable<T>
        {
            while (true)
            {
                var pivotIdx = source.Partition(start, end, random);
                if (pivotIdx == n) return source[pivotIdx];

                if (n < pivotIdx) end = pivotIdx - 1;
                else start = pivotIdx + 1;
            }
        }

        /// <summary>
        /// Calculates the median of source in O(n) time
        /// </summary>
        public static double Median<T>(this IEnumerable<T> source, Func<T, double> map)
        {
            var arr = source.Select(map).ToArray();
            if (arr.Length == 0) throw new ArgumentOutOfRangeException(nameof(source), "Source does not have any items");
            var mid = (arr.Length - 1) / 2;
            return arr.NthOrderStatistic(mid, 0, arr.Length - 1, null);
        }
    }
}