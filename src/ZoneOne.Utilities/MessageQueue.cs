using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ZoneOne.Utilities
{
    public class MessageQueue<T>
    {
        private readonly CancellationToken? _cancel;
        private TaskCompletionSource<T> _nextItem = new();

        public MessageQueue(CancellationToken? cancel = default)
        {
            _cancel = cancel;
        }

        public void Add(T item)
        {
            var oldNext = _nextItem;
            _nextItem = new TaskCompletionSource<T>();
            oldNext.TrySetResult(item);
        }

        public async IAsyncEnumerable<T> Read(CancellationToken? cancel = default)
        {
            while (_cancel?.IsCancellationRequested != true && cancel?.IsCancellationRequested != true)
                yield return await _nextItem.Task;
        }
    }
}