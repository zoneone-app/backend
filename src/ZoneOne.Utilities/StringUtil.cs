using System;

namespace ZoneOne.Utilities
{
    public static class StringUtil
    {
        public static string SubstringBefore(this string source, string characters)
        {
            var index = source.IndexOf(characters, StringComparison.Ordinal);
            return index == -1 ? source : source.Substring(0, index);
        }

        public static string? SubstringAfter(this string source, string characters)
        {
            var index = source.IndexOf(characters, StringComparison.Ordinal);
            return index == -1 ? null : source.Substring(index + characters.Length);
        }

        public static string DefaultTo(this string source, string def)
        {
            return string.IsNullOrEmpty(source) ? def : source;
        }
    }
}