using System;
using System.Linq;
using ZoneOne.Static.Data.Util;

namespace ZoneOne.Utilities
{
    public static class DateUtil
    {
        private static readonly DateTime DefaultEpoch = new DateTime(2021, 1, 1);

        /// <summary>
        /// Week day of the epoch (assuming Monday=0)
        /// </summary>
        public const int EpochWeekday = 4;

        public static DateTime GetDateFromEpoch(int days, DateTime epoch)
        {
            return epoch + TimeSpan.FromDays(days);
        }

        public static DateTime GetDateFromEpoch(int days)
        {
            return GetDateFromEpoch(days, DefaultEpoch);
        }

        public static int GetEpochFromDate(DateTime date, DateTime epoch)
        {
            return (date - epoch).Days;
        }

        public static int GetEpochFromDate(DateTime date)
        {
            return GetEpochFromDate(date, DefaultEpoch);
        }

        public static TimeSpan ParseAnyHourDate(string source)
        {
            var times = source.Split(':');
            var hour = Convert.ToInt32(times[0]);
            var minute = Convert.ToInt32(times[1]);
            var second = (times.Length == 3) ? Convert.ToInt32(times[2]) : 0;

            var day = hour / 24;
            hour %= 24;

            return new TimeSpan(day, hour, minute, second);
        }

        public static Days GetDay(int weekday) => (weekday % 7) switch
        {
            0 => Days.Monday,
            1 => Days.Tuesday,
            2 => Days.Wednesday,
            3 => Days.Thursday,
            4 => Days.Friday,
            5 => Days.Saturday,
            6 => Days.Sunday,
            _ => throw new ArgumentOutOfRangeException(nameof(weekday))
        };

        public static Days GetDaysBetween(int startWeekDay, int endWeekDay)
        {
            var nextWeekEnd = endWeekDay < startWeekDay ? endWeekDay + 7 : endWeekDay;

            return Enumerable.Range(startWeekDay, nextWeekEnd - startWeekDay + 1)
                .Aggregate((Days) 0, (prev, curr) => prev | GetDay(curr));
        }
    }
}