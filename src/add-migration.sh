#!/bin/bash

echo Creating migration $1
dotnet ef migrations add -c StaticDbContext -p ZoneOne.Static.Database -s ZoneOne.Backend $1
