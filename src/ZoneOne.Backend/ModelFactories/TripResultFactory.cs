using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ZoneOne.Backend.Extensions;
using ZoneOne.Backend.Models;
using ZoneOne.Static.Data;
using ZoneOne.Static.Database;

namespace ZoneOne.Backend.ModelFactories
{
    public class TripResultFactory
    {
        private readonly StaticDbContext _db;

        public TripResultFactory(StaticDbContext db)
        {
            _db = db;
        }

        public TripResult GetTrip(Trip trip, Route route)
        {
            return new()
            {
                Id = trip.Id.ToBase32(),
                RouteId = trip.StoppingPattern.RouteId.ToBase32(),
                StoppingPatternId = trip.StoppingPatternId.ToBase32(),
                StartTime = trip.StartTime,
                Stops =
                    from ps in trip.StoppingPattern.PatternStops.AsQueryable()
                    let timing = trip.PatternTiming.Timings.First(v => v.PositionInSequence == ps.PositionInSequence)
                    let timingPoint = trip.StoppingPattern.Route.TimingPoints.FirstOrDefault(v => v.StopId == ps.StopId)
                    let overriddenStopId = (
                        from stopOverride in route.PatternStopOverrides
                        where stopOverride.ExistingStopId == ps.StopId
                        select (Guid?) stopOverride.NewStopId
                    ).FirstOrDefault() ?? ps.StopId
                    orderby ps.PositionInSequence
                    select new TripStopInfo
                    {
                        StopId = overriddenStopId.ToBase32(),
                        Dwell = timing.DwellDuration,
                        ArrivalOffset = timing.TimeToStop,
                        IsTimingPoint = timingPoint != null && timingPoint.IsTimingPoint
                    }
            };
        }

        public async Task<TripResult> GetTrip(Guid id)
        {
            var trip = await _db.Trips
                .AsQueryable()
                .Include(v => v.StoppingPattern)
                    .ThenInclude(v => v.Route)
                .FirstAsync(v => v.Id == id);
            var route = trip.StoppingPattern.Route;

            return GetTrip(trip, route);
        }
    }
}