namespace ZoneOne.Backend.Extensions
{
    public static class NumberExtensions
    {
        public static bool Between(this float num, float min, float max)
        {
            if (max < min) return num >= max && num <= min;
            return num >= min && num <= max;
        }

        /// <summary>
        /// Converts a number between min and max to a number between 0 and 1
        /// </summary>
        public static float Unmap(this float num, float min, float max) => (num - min) / (max - min);
    }
}