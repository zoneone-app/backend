using System;
using SimpleBase;

namespace ZoneOne.Backend.Extensions
{
    public static class GuidExtensions
    {
        public static string ToBase32(this Guid guid) => Base32.Crockford.Encode(guid.ToByteArray()).ToLowerInvariant();
        public static Guid FromBase32(this string guid)
        {
            var res = new Span<byte>(new byte[16]);

            if (Base32.Crockford.TryDecode(guid.ToUpperInvariant(), res, out _))
                return new Guid(res);

            throw new InvalidOperationException("Not valid Base32");
        }
    }
}