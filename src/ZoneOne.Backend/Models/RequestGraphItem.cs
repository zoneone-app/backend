using System;

namespace ZoneOne.Backend.Models
{
    public record RequestGraphItem(DateTime Time, double Count, double MedDurationMs);
}