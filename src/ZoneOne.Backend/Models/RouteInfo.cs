using ZoneOne.Static.Data;

namespace ZoneOne.Backend.Models
{
    public record RouteInfo
    {
        public string Id { get; init; }
        
        public string Code { get; init; }
        public string Title { get; init; }
        public string Description { get; init; }
        
        public Route.RouteMode? Mode { get; init; }
        public Route.RouteStyling? Styling { get; init; }
        public Route.RouteDirectionType? DirectionType { get; init; }
        public Route.RouteDirection? Direction { get; init; }
        public Route.RouteBranding? Branding { get; init; }
        public Route.RouteStoppingPatternType? StoppingPatternType { get; init; }
        
        public bool? HailRide { get; init; }
        public bool? Free { get; init; }
        public bool? School { get; init; }
    }
}