namespace ZoneOne.Backend.Models
{
    public record ImporterState(bool IsImporting, bool IsWaiting);
}