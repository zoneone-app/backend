using System.Collections.Generic;

namespace ZoneOne.Backend.Models
{
    public record ApiRoute
    {
        public string Name { get; init; }
        public string Method { get; init; }
        public string Template { get; init; }
        public IEnumerable<string> RequiredScopes { get; init; }
        public bool RequiresAuth { get; init; }
    }
}