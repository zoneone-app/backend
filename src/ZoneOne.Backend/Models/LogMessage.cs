using System;
using Microsoft.Extensions.Logging;

namespace ZoneOne.Backend.Models
{
    public record LogMessage(
        DateTime Time,
        EventId Event,
        LogLevel Level,
        string Name,
        string Message
    );
}