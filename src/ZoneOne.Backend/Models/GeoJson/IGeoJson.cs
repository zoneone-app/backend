namespace ZoneOne.Backend.Models.GeoJson
{
    public interface IGeoJson
    {
        public string Type { get; }
    }
}