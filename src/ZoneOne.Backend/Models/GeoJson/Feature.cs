using System.Collections.Generic;
using ZoneOne.Backend.Models.GeoJson.Geometry;

namespace ZoneOne.Backend.Models.GeoJson
{
    public record Feature(IGeometry<object> Geometry, IReadOnlyDictionary<string, object> Properties)
    {
        public string Type => "Feature";
    }
}