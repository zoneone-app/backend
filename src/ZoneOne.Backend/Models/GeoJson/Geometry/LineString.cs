using System.Collections.Generic;

namespace ZoneOne.Backend.Models.GeoJson.Geometry
{
    public record LineString(IEnumerable<double[]> Coordinates) : IGeometry<double[]>
    {
        public string Type => "LineString";
    }
}