using System.Collections.Generic;

namespace ZoneOne.Backend.Models.GeoJson.Geometry
{
    public interface IGeometry<out T> : IGeoJson
    {
        public IEnumerable<T> Coordinates { get; }
    }
}