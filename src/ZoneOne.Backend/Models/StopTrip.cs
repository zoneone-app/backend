using ZoneOne.Static.Data.Util;

namespace ZoneOne.Backend.Models
{
    public record StopTrip
    {
        public string TripId { get; init; }
        public string RouteId { get; init; }
        public string StoppingPatternId { get; set; }

        public TripResult Trip { get; set; }
        

        /// <summary>
        /// The time the vehicle arrives at its stop, in minutes since the day start
        /// </summary>
        public int ArrivalTime { get; init; }


        /// <summary>
        /// The amount of time the vehicle stays at the stop, in minutes
        /// </summary>
        public int DwellDuration { get; init; }

        /// <summary>
        /// The days the trip runs on
        /// </summary>
        public Days Days { get; init; }
    }
}