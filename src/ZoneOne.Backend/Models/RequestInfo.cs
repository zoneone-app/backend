namespace ZoneOne.Backend.Models
{
    public record RequestInfo(
        double DurationMs
    );
}