using Microsoft.AspNetCore.Mvc;

namespace ZoneOne.Backend.Models
{
    [ApiController]
    [Route("subscription")]
    public class SubscriptionController : Controller
    {
        [HttpGet("")]
        public IActionResult SubscriptionEvents()
        {
            return BadRequest("must be Accept: text/event-stream");
        }
    }
}