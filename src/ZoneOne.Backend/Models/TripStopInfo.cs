namespace ZoneOne.Backend.Models
{
    public record TripStopInfo
    {
        public string StopId { get; init; }
        
        /// <summary>
        /// Offset since the trip start time that it arrives at this stop
        /// </summary>
        public short ArrivalOffset { get; init; }
        
        /// <summary>
        /// Vehicle always be at this stop at the specified time
        /// </summary>
        public bool IsTimingPoint { get; init; }
        
        /// <summary>
        /// The amount of minutes the vehicle waits at this stop
        /// </summary>
        public short Dwell { get; init; }
    }
}