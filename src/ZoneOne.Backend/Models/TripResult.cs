using System.Collections.Generic;

namespace ZoneOne.Backend.Models
{
    public record TripResult
    {
        public string Id { get; init; }
        public string RouteId { get; init; }
        public string StoppingPatternId { get; init; }

        public short StartTime { get; init; }

        public IEnumerable<TripStopInfo> Stops { get; init; }
    }
}