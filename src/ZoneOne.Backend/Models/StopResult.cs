using ZoneOne.Static.Data.Util;

namespace ZoneOne.Backend.Models
{
    public record StopResult
    {
        public string Id { get; init; }
        public int GtfsId { get; set; }
        public string Name { get; init; }
        public string? Suburb { get; init; }
        public Zone Zone { get; init; }
        public Zone? OtherZone { get; init; }
        public bool IsDummy { get; init; }
        public string? StationId { get; init; }
    }
}