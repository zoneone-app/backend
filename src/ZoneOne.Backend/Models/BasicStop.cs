using System;

namespace ZoneOne.Backend.Models
{
    public class BasicStop
    {
        public Guid Id { get; }

        public float Longitude { get; init; }
        public float Latitude { get; init; }

        public BasicStop(Guid id)
        {
            Id = id;
        }
    }
}