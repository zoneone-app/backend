using System.Collections.Generic;

namespace ZoneOne.Backend.Models
{
    public record StopTripResult(int Count, IEnumerable<StopTrip> Trips);
}