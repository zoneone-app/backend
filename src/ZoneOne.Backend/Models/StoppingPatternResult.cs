using ZoneOne.Static.Data;

namespace ZoneOne.Backend.Models
{
    public record StoppingPatternResult
    {
        public string Id { get; init; }
        public string RouteId { get; init; }
        
        /// <summary>
        /// Direction that the stopping pattern goes, depending on the DirectionType in routes
        /// </summary>
        public StoppingPattern.Dir Direction { get; init; }
        
        /// <summary>
        /// Variation at the start of the route
        /// </summary>
        public StoppingPattern.Variation StartVariation { get; init; }
        
        /// <summary>
        /// Variation in the middle of the route
        /// </summary>
        public StoppingPattern.ExtendedVariation MiddleVariation { get; init; }
        
        /// <summary>
        /// Variation at the end of the route
        /// </summary>
        public StoppingPattern.Variation EndVariation { get; init; }

        public string? VariationLabel { get; init; }
    }
}