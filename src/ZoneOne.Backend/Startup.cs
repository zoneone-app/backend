using System;
using System.IO;
using System.Reflection;
using Lib.AspNetCore.ServerSentEvents;
using MessagePipe;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.WebSockets;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Quartz;
using ZoneOne.Backend.Auth;
using ZoneOne.Backend.BackgroundServices;
using ZoneOne.Backend.Jobs;
using ZoneOne.Backend.Middleware;
using ZoneOne.Backend.Realtime;
using ZoneOne.Backend.ServiceExtensions;
using ZoneOne.Static.Database;
using ZoneOne.Static.Importer;

namespace ZoneOne.Backend
{
    public class Startup
    {
        private const string ZoneOneCors = "_zoneOneCors";
        private const string LocalhostCors = "_devCors";
        
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<StaticDbContext>();
            
            services.AddMessagePipe();
            services.AddServerSentEvents();

            services.AddConfiguration();
            
            services.AddSingleton<ServiceStatus>();
            services.AddHostedService<MigrationsBackgroundService>();

            services.AddRedis(Configuration.GetSection("Redis"));

            services.AddTransient<IStaticDataImporter, StaticDataImporter>();

            services.AddHttpClient("importer", opts =>
            {
                var assembly = Assembly.GetAssembly(typeof(StaticDataImporter));
                if (assembly == null) throw new NullReferenceException("Static data importer assembly is null");

                var version = assembly.GetName();

                var userAgent = $"{version.Name}/{version.Version} (contact+importer@zoneone.app)";
                
                opts.DefaultRequestHeaders.Add("User-Agent", userAgent);
            });
            
            services.AddImporterTriggerer();

            services.AddQuartz(v =>
            {
                v.UseMicrosoftDependencyInjectionScopedJobFactory();

                v.AddImporterJob(Configuration.GetSection("Importer"));
            });

            services.AddQuartzHostedService();

            services.AddTransient<StateValueMiddleware>();
            services.AddTransient<DashboardRequestMiddleware>();

            services.AddResultFactories();

            services.AddCors(cors =>
            {
                cors.AddPolicy(ZoneOneCors, builder =>
                {
                    builder
                        .WithOrigins("https://zoneone.app", "https://dashboard.zoneone.app")
                        .WithHeaders("X-Requested-With", "X-SignalR-User-Agent", "Authorization")
                        .WithMethods("GET", "POST", "PUT")
                        .AllowCredentials();
                });
                
                cors.AddPolicy(LocalhostCors, builder =>
                {
                    builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod();
                });
            });

            services.AddResponseCaching();

            services.AddControllers().AddNewtonsoftJson();

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(options =>
            {
                options.Authority = "https://zoneone.au.auth0.com";
                options.Audience = "https://api.zoneone.app";
            });

            services.AddSingleton<IAuthorizationHandler, HasScopeHandler>();

            services.AddAuthorization(options =>
            {
                options.AddScopedPolicy(ScopeNames.CreateDataImport);
            });
            
            services.AddSignalR(cfg =>
            {
                cfg.EnableDetailedErrors = true;
            }).AddMessagePackProtocol();
            
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "ZoneONE Static",
                    Version = "v1",
                    Description = "API for static transit data",
                    Contact = new OpenApiContact
                    {
                        Name = "ZoneONE Admins",
                        Email="contact+staticapi@zoneone.app"
                    },
                    License = new OpenApiLicense
                    {
                        Name = "MIT",
                        Url = new Uri("https://opensource.org/licenses/MIT")
                    }
                });
                
                c.IncludeXmlComments(Path.Combine(
                    AppContext.BaseDirectory,
                    $"{Assembly.GetExecutingAssembly().GetName().Name}.xml"
                ));
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseMiddleware<DashboardRequestMiddleware>();
            app.UseMiddleware<StateValueMiddleware>();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "ZoneOne.Backend v1"));
            }

            app.UseRouting();

            app.UseCors(env.IsDevelopment() ? LocalhostCors : ZoneOneCors);

            app.UseResponseCaching();
            
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapServerSentEvents("/subscription");
                endpoints.MapControllers();

                endpoints.MapHub<DashboardHub>("/rt/dash");
            });
        }
    }
}