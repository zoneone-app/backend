using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Quartz;
using ZoneOne.Backend.BackgroundServices;
using ZoneOne.Static.Importer;

namespace ZoneOne.Backend.Jobs
{
    public static class ImporterJobExtensions
    {
        public static void AddImporterJob(this IServiceCollectionQuartzConfigurator services, IConfiguration configuration)
        {
            var scheduleConfig = configuration.GetValue<string>("Schedule");

            var triggerJobTriggerKey = new TriggerKey("trigger trigger", "importer");
            var starterJobTriggerKey = new TriggerKey("starter trigger", "importer");

            services.AddJob<ImporterStarterJob>(opts =>
                opts.WithIdentity(ImporterStarterJob.JobKey)
                    .WithDescription("Begins an import if one is pending"));

            services.AddTrigger(opts => opts
                .ForJob(ImporterStarterJob.JobKey)
                .WithIdentity(starterJobTriggerKey)
                .WithDescription("Triggers the importer starter to run every 30 seconds")
                .WithSimpleSchedule(builder => builder.WithIntervalInSeconds(30).RepeatForever())
                .StartNow());
            
            if (configuration.GetValue<bool>("Enabled") == false) return;

            services.AddJob<ImporterTriggerJob>(opts => 
                opts.WithIdentity(ImporterTriggerJob.JobKey)
                    .WithDescription("Sets the importer to be pending"));
            
            if (scheduleConfig == "_RUN_ONCE")
            {
                services.AddTrigger(opts => opts
                    .ForJob(ImporterTriggerJob.JobKey)
                    .WithIdentity(triggerJobTriggerKey)
                    .WithDescription("Importer trigger to run instantly, once")
                    .StartNow()
                );
                return;
            }

            services.AddTrigger(opts => opts
                .ForJob(ImporterTriggerJob.JobKey)
                .WithIdentity(triggerJobTriggerKey)
                .WithDescription($"Importer trigger to run on cron schedule: `{scheduleConfig}`")
                .WithCronSchedule(CronScheduleBuilder.CronSchedule(scheduleConfig))
                .StartNow());
        }

        public static void AddImporterTriggerer(this IServiceCollection services)
        {
            services.AddTransient<ImporterRunner>();
        }
    }
    
    public class ImporterRunner
    {
        private readonly ILogger<ImporterRunner> _logger;
        private readonly ServiceStatus _serviceStatus;
        private readonly IStaticDataImporter _staticDataImporter;

        public ImporterRunner(ILogger<ImporterRunner> logger, ServiceStatus serviceStatus,
            IStaticDataImporter staticDataImporter)
        {
            _logger = logger;
            _serviceStatus = serviceStatus;
            _staticDataImporter = staticDataImporter;
        }

        public async Task ExecuteAsync()
        {
            if (_serviceStatus.Migration.IsRunning)
            {
                _logger.LogInformation("Waiting for migrations to complete");
                await _serviceStatus.Migration.StoppedTask;
            }
            
            _logger.LogInformation("Running importer");

            _serviceStatus.Importer.SetRunning();
            try
            {
                await _staticDataImporter.RunAsync();
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, "Importer failed to complete");
            }
            finally
            {
                _serviceStatus.Importer.SetStopped();
            }
        }
    }

    [DisallowConcurrentExecution]
    public class ImporterStarterJob : IJob
    {
        internal static readonly JobKey JobKey = new("starter job", "importer");
        
        private readonly ImporterRunner _runner;
        private readonly ServiceStatus _status;
        private readonly ILogger<ImporterStarterJob> _logger;

        public ImporterStarterJob(ImporterRunner runner, ServiceStatus status, ILogger<ImporterStarterJob> logger)
        {
            _runner = runner;
            _status = status;
            _logger = logger;
        }

        public Task Execute(IJobExecutionContext context)
        {
            _logger.LogDebug("Checking if the importer should run");
            return _status.Importer.IsPending ? _runner.ExecuteAsync() : Task.CompletedTask;
        }
    }

    public class ImporterTriggerJob : IJob
    {
        private readonly ServiceStatus _status;
        internal static readonly JobKey JobKey = new("trigger job", "importer");

        public ImporterTriggerJob(ServiceStatus status)
        {
            _status = status;
        }

        public Task Execute(IJobExecutionContext context)
        {
            _status.Importer.SetPending();
            return Task.CompletedTask;
        }
    }
}