using System;
using System.Threading;
using System.Threading.Tasks;
using MessagePipe;
using Microsoft.Extensions.Logging;

namespace ZoneOne.Backend.BackgroundServices
{
    public record StatusState<T>(T Id, IStatusStateProvider State);
    
    public sealed class Status<T> : IDisposable
    {
        private class CompletionSources
        {
            private TaskCompletionSource _tcs = new();
            private CancellationTokenSource _cts = new();

            private void TriggerTcs()
            {
                var old = _tcs;
                _tcs = new TaskCompletionSource();
                old.TrySetResult();
            }

            private void TriggerCts()
            {
                var old = _cts;
                _cts = new CancellationTokenSource();
                old.Cancel();
            }

            private void TriggerEvent()
            {
                Event?.Invoke(this, new EventArgs());
            }

            public void Trigger()
            {
                TriggerTcs();
                TriggerCts();
                TriggerEvent();
            }

            public Task Task => _tcs.Task;
            public CancellationToken CancellationToken => _cts.Token;
            public event EventHandler? Event;
        }

        private static bool IsRunningState(StatusState state) => state is StatusState.Pending or StatusState.Running;
        private static bool IsStoppedState(StatusState state) => state is StatusState.Cancelling or StatusState.Stopped;

        private readonly IStatusStateProvider _stateProvider;

        private readonly CompletionSources _cancellingCs = new();
        private readonly CompletionSources _stoppedCs = new();
        private readonly CompletionSources _pendingCs = new();
        private readonly CompletionSources _runningCs = new();

        private readonly bool _supportsPending;

        private readonly T _id;
        private readonly IDisposablePublisher<StatusState<T>> _statePublisher;
        
        private readonly ILogger _logger;
        
        public ISubscriber<StatusState<T>> OnStateChanged { get; }

        private EventHandler HandleChanged(EventHandler? ev)
        {
            return (sender, args) =>
            {
                _logger.LogDebug("Value changed in status for {Id}; now {NewState}", _id, _stateProvider);
                ev?.Invoke(sender, args);
                StatusChanged?.Invoke(sender, args);
                _statePublisher.Publish(new StatusState<T>(_id, _stateProvider));
            };
        }

        internal Status(ILogger logger, T id, EventFactory eventFactory, IStatusStateProvider stateProvider, bool supportsPending = true)
        {
            _logger = logger;
            _id = id;
            _stateProvider = stateProvider;
            _supportsPending = supportsPending;

            _stateProvider.WriteDefault(StatusState.Stopped);

            _cancellingCs.Event += HandleChanged(CancellingEvent);
            _stoppedCs.Event += HandleChanged(StoppedEvent);
            _pendingCs.Event += HandleChanged(PendingEvent);
            _runningCs.Event += HandleChanged(RunningEvent);

            (_statePublisher, OnStateChanged) = eventFactory.CreateEvent<StatusState<T>>();
        }

        public void Dispose()
        {
            _statePublisher.Dispose();
        }

        public void SetCancelling()
        {
            switch (_stateProvider.Read())
            {
                case StatusState.Cancelling or StatusState.Stopped:
                    return;
                case StatusState.Pending:
                    SetStopped();
                    break;
                case StatusState.Running:
                    _stateProvider.Write(StatusState.Cancelling);
                    _cancellingCs.Trigger();
                    break;
            }
        }

        // Sets to stopped, or cancels the pending state
        public void SetStopped()
        {
            if (_stateProvider.Read() == StatusState.Stopped) return;

            _stateProvider.Write(StatusState.Stopped);
            _stoppedCs.Trigger();
        }

        /// <summary>
        /// If not already running, sets the status state to be pending, and triggers <see cref="PendingTask"/>
        /// </summary>
        /// <remarks>State consumer can check <see cref="IsPending"/> to set the state to running</remarks>
        /// <remarks>Triggers <see cref="RunningTask"/> if the state changed</remarks>
        /// <exception cref="InvalidOperationException">Pending is not supported</exception>
        public void SetPending()
        {
            if (!_supportsPending) throw new InvalidOperationException("Pending is not supported");
            if (IsRunningState(_stateProvider.Read())) return;

            _stateProvider.Write(StatusState.Pending);
            _pendingCs.Trigger();
        }

        /// <summary>
        /// Sets the state to be currently running
        /// </summary>
        public void SetRunning()
        {
            if (_stateProvider.Read() == StatusState.Running) return;

            _stateProvider.Write(StatusState.Running);
            _runningCs.Trigger();
        }

        public bool IsPending => _stateProvider.Read() == StatusState.Pending;
        public bool IsRunning => _stateProvider.Read() == StatusState.Running;
        public bool IsStopped => _stateProvider.Read() == StatusState.Stopped;
        public bool IsCancelling => _stateProvider.Read() == StatusState.Cancelling;

        /// <summary>
        /// Task completes when the state changes to be cancelling
        /// </summary>
        public Task CancellingTask => _cancellingCs.Task;

        /// <summary>
        /// Task completes when the state changes to be stopped
        /// </summary>
        public Task StoppedTask => _stoppedCs.Task;

        /// <summary>
        /// Task completes when the state changes to be pending
        /// </summary>
        public Task PendingTask => _pendingCs.Task;

        /// <summary>
        /// Task completes when the state changes to be running
        /// </summary>
        public Task RunningTask => _runningCs.Task;

        /// <summary>
        /// Cancels when the state changes to be cancelling
        /// </summary>
        public CancellationToken CancellingToken => _cancellingCs.CancellationToken;

        /// <summary>
        /// Cancels when the state changes to be stopped
        /// </summary>
        public CancellationToken StoppedToken => _stoppedCs.CancellationToken;

        /// <summary>
        /// Cancels when the state changes to be pending
        /// </summary>
        public CancellationToken PendingToken => _pendingCs.CancellationToken;

        /// <summary>
        /// Cancels when the state changes to be running
        /// </summary>
        public CancellationToken RunningToken => _runningCs.CancellationToken;

        /// <summary>
        /// Invokes when the state changes to be cancelling
        /// </summary>
        public event EventHandler? CancellingEvent;

        /// <summary>
        /// Invokes when the state changes to be stopped
        /// </summary>
        public event EventHandler? StoppedEvent;

        /// <summary>
        /// Invokes when the state changes to be pending
        /// </summary>
        public event EventHandler? PendingEvent;

        /// <summary>
        /// Invokes when the state changes to be running
        /// </summary>
        public event EventHandler? RunningEvent;

        public event EventHandler? StatusChanged;
    }
}