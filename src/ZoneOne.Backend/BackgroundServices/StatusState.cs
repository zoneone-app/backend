namespace ZoneOne.Backend.BackgroundServices
{
    public enum StatusState
    {
        Cancelling,
        Stopped,
        Pending,
        Running
    }
}