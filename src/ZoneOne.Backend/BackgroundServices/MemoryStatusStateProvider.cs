using System;

namespace ZoneOne.Backend.BackgroundServices
{
    public class MemoryStatusStateProvider : IStatusStateProvider
    {
        private StatusState? _value;
        
        public void Write(StatusState state)
        {
            _value = state;
        }

        public void WriteDefault(StatusState state)
        {
            _value ??= state;
        }

        public StatusState Read()
        {
            if (_value == null) throw new NullReferenceException("No status has been written");
            return (StatusState)_value;
        }
    }
}