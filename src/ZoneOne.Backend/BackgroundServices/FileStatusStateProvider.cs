using System;
using System.IO;

namespace ZoneOne.Backend.BackgroundServices
{
    /// <summary>
    /// Stores the status state permanently in a file
    /// </summary>
    public class FileStatusStateProvider : IStatusStateProvider
    {
        private readonly string _filePath;

        public FileStatusStateProvider(string filePath)
        {
            _filePath = filePath;
        }

        public void Write(StatusState state)
        {
            File.WriteAllText(_filePath, state.ToString());
        }

        public void WriteDefault(StatusState state)
        {
            if (!File.Exists(_filePath)) Write(state);
        }

        public StatusState Read()
        {
            if (!File.Exists(_filePath)) throw new NullReferenceException("No status has been written");
            return Enum.Parse<StatusState>(File.ReadAllText(_filePath));
        }
    }
}