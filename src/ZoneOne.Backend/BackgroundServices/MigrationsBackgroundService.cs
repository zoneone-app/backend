using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Npgsql;
using ZoneOne.Static.Database;

namespace ZoneOne.Backend.BackgroundServices
{
    public class MigrationsBackgroundService : BackgroundService
    {
        private readonly ILogger<MigrationsBackgroundService> _logger;
        private readonly IServiceProvider _serviceProvider;
        private readonly ServiceStatus _serviceStatus;

        public MigrationsBackgroundService(ILogger<MigrationsBackgroundService> logger, 
            IServiceProvider services, ServiceStatus serviceStatus)
        {
            _logger = logger;
            _serviceProvider = services;
            _serviceStatus = serviceStatus;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _serviceStatus.Migration.SetRunning();

            using var scope = _serviceProvider.CreateScope();
            var dbContext = scope.ServiceProvider.GetService<StaticDbContext>() ??
                            throw new NullReferenceException("Database context does not exist");
            
            _logger.LogDebug("Getting pending migrations");
            var pending = (await dbContext.Database.GetPendingMigrationsAsync(stoppingToken))?.ToArray();
            
            if (pending?.Length == 1) _logger.LogDebug("There is 1 pending migration");
            else _logger.LogDebug("There are {0} pending migrations", pending?.Length);

            if (pending?.Length < 1)
            {
                _logger.LogDebug("Skipping migrations as there are none pending");
                _serviceStatus.Migration.SetStopped();
                return;
            }

            _logger.LogInformation("Running database migrations");
            
            await dbContext.Database.MigrateAsync(stoppingToken);

            _serviceStatus.Migration.SetStopped();

            _logger.LogInformation("Finished running database migrations");
        }
    }
}