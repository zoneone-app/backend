using System;
using Castle.Core.Configuration;
using MessagePipe;
using Microsoft.Extensions.Logging;
using ZoneOne.Backend.Configuration;

namespace ZoneOne.Backend.BackgroundServices
{
    public enum StatusType
    {
        Migration,
        Importer
    }
    
    public sealed class ServiceStatus : IDisposable
    {
        public ServiceStatus(ILogger<ServiceStatus> logger, EventFactory eventFactory, ImporterConfiguration importerConfig)
        {
            Migration = new Status<StatusType>(logger, StatusType.Migration, eventFactory, new MemoryStatusStateProvider(), false);
            Importer = new Status<StatusType>(logger, StatusType.Importer, eventFactory, new FileStatusStateProvider(importerConfig.StateFile));
        }

        public Status<StatusType> Migration { get; }
        public Status<StatusType> Importer { get; }

        public void Dispose()
        {
            Migration.Dispose();
            Importer.Dispose();
        }
    }
}