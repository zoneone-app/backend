using System;

namespace ZoneOne.Backend.BackgroundServices
{
    public interface IStatusStateProvider
    {
        /// <summary>
        /// Write the state to the provider, so that it can be read out later
        /// </summary>
        void Write(StatusState state);
        /// <summary>
        /// Write the state to the provider if one has not been set already
        /// </summary>
        void WriteDefault(StatusState state);
        /// <summary>
        /// Read the state back out
        /// </summary>
        /// <exception cref="NullReferenceException">No status has been written</exception>
        StatusState Read();
    }
}