using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ZoneOne.Backend.Auth;
using ZoneOne.Backend.BackgroundServices;
using ZoneOne.Backend.Middleware;
using ZoneOne.Backend.Models;

namespace ZoneOne.Backend.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Produces("application/json")]
    [ProducesErrorResponseType(typeof(ISvm<HttpError>))]
    [Svm(SvmMode.AlwaysOn)]
    public class DashboardController : Controller
    {
        private readonly ServiceStatus _serviceStatus;
        private readonly ILogger<DashboardController> _logger;

        public DashboardController(ServiceStatus serviceStatus, ILogger<DashboardController> logger)
        {
            _serviceStatus = serviceStatus;
            _logger = logger;
        }

        [HttpGet("importer-state")]
        public IActionResult GetImporterState()
        {
            return Ok(new ImporterState(_serviceStatus.Importer.IsRunning, _serviceStatus.Importer.IsPending || _serviceStatus.Importer.IsCancelling));
        }

        [HttpPut("importer-state")]
        [Authorize(ScopeNames.CreateDataImport)]
        [ProducesResponseType(204)]
        public IActionResult SetImporterState([FromForm] ImporterState state)
        {
            if (state.IsImporting == !_serviceStatus.Importer.IsStopped)
                return BadRequest(new HttpError(nameof(state.IsImporting), "Already in that state"));

            if (state.IsImporting)
            {
                _logger.LogInformation("Triggering importer to run manually");
                _serviceStatus.Importer.SetPending();
            }
            else
            {
                _logger.LogInformation("Cancelling importer");
                _serviceStatus.Importer.SetCancelling();
            }

            return NoContent();
        }
    }
}