using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ZoneOne.Backend.Extensions;
using ZoneOne.Backend.Middleware;
using ZoneOne.Backend.Models;
using ZoneOne.Static.Database;

namespace ZoneOne.Backend.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Produces("application/json")]
    [ProducesErrorResponseType(typeof(ISvm<HttpError>))]
    public class RoutesController : Controller
    {
        private readonly StaticDbContext _db;

        public RoutesController(StaticDbContext db)
        {
            _db = db;
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(ISvm<RouteInfo>), 200)]
        public async Task<IActionResult> GetRoute(string id)
        {
            var guid = id.FromBase32();

            if (guid == null) return HttpError.InvalidGuid(this, nameof(id));
            if (!await _db.Routes.AsQueryable().AnyAsync(it => it.Id == guid)) return NotFound();
            var route = await _db.Routes.AsQueryable().FirstAsync(it => it.Id == guid);

            return Ok(new RouteInfo
            {
                Id = id,
                
                Code = route.Code,
                Title = route.Title,
                Description = route.Description,
                
                Mode = route.Mode,
                Styling = route.Styling,
                DirectionType = route.DirectionType,
                Direction = route.Direction,
                Branding = route.Branding,
                StoppingPatternType = route.StoppingPatternType,
                
                HailRide = route.HailRide,
                Free = route.Free,
                School = route.School
            });
        }
    }
}