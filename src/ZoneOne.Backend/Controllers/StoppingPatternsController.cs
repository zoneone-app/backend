using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ZoneOne.Backend.Extensions;
using ZoneOne.Backend.Middleware;
using ZoneOne.Backend.Models;
using ZoneOne.Backend.Models.GeoJson;
using ZoneOne.Backend.Models.GeoJson.Geometry;
using ZoneOne.Static.Data;
using ZoneOne.Static.Data.Util;
using ZoneOne.Static.Database;
using ZoneOne.Utilities;

namespace ZoneOne.Backend.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Produces("application/json")]
    [ProducesErrorResponseType(typeof(ISvm<HttpError>))]
    public class StoppingPatternsController : Controller
    {
        private readonly StaticDbContext _db;
        
        public StoppingPatternsController(StaticDbContext db)
        {
            _db = db;
        }

        /// <summary>
        /// Gets information about a specific stopping pattern a route takes
        /// </summary>
        /// <param name="id">ID of the pattern</param>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(ISvm<StoppingPatternResult>), 200)]
        public async Task<IActionResult> GetStoppingPattern(string id)
        {
            var guid = id.FromBase32();

            if (guid == null) return HttpError.InvalidGuid(this, nameof(id));
            if (!await _db.StoppingPatterns.AsQueryable().AnyAsync(v => v.Id == guid)) return NotFound();
            var stoppingPattern = await _db.StoppingPatterns.AsQueryable().FirstAsync(v => v.Id == guid);

            return Ok(new StoppingPatternResult
            {
                Id = stoppingPattern.Id.ToBase32(),
                RouteId = stoppingPattern.RouteId.ToBase32(),
                Direction = stoppingPattern.Direction,
                VariationLabel = stoppingPattern.StoppingPatternLabel,
                StartVariation = stoppingPattern.StartVariation,
                MiddleVariation = stoppingPattern.MiddleVariation,
                EndVariation = stoppingPattern.EndVariation
            });
        }

        /// <summary>
        /// Gets the path that this stopping pattern takes, in GeoJSON format
        /// </summary>
        /// <param name="id">ID of the stopping pattern</param>
        [HttpGet("{id}/path")]
        [ProducesResponseType(typeof(ISvm<IFeature>), 200)]
        public async Task<IActionResult> GetPath(string id)
        {
            var guid = id.FromBase32();
            
            if (guid == null) return HttpError.InvalidGuid(this, nameof(id));
            if (!await _db.StoppingPatterns.AsQueryable().AnyAsync(v => v.Id == guid)) return NotFound();
            var stoppingPattern = await _db.StoppingPatterns.AsQueryable().SingleAsync(v => v.Id == guid);

            var stops =
                from patternStop in stoppingPattern.PatternStops
                orderby patternStop.PositionInSequence
                select patternStop.Stop;

            var geometry = await stops
                .Pair()
                .ToAsyncEnumerable()
                .SelectAwait(GetStopsPath)
                .SelectMany(v => v.Select(SplitCoordinate).ToAsyncEnumerable())
                .ToArrayAsync();

            var feature = new Feature(
                new LineString(
                    geometry
                ),
                new Dictionary<string, object>()
            );

            return Ok(feature);
        }

        private static double[] SplitCoordinate(Coordinate v)
        {
            var (longitude, latitude) = v;
            return new double[] {longitude, latitude};
        }

        private async ValueTask<IEnumerable<Coordinate>> GetStopsPath((Stop, Stop) pair)
        {
            var (a, b) = pair;

            var path = await _db.SegmentStops.AsQueryable()
                .SingleOrDefaultAsync(v => v.StartStopId == a.Id && v.EndStopId == b.Id);

            return path?.Path ?? Array.Empty<Coordinate>();
        }
    }
}