using Microsoft.AspNetCore.Mvc;
using ZoneOne.Backend.Middleware;

namespace ZoneOne.Backend.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public sealed class SubscriptionController : ControllerBase
    {
        [HttpGet("")]
        [Svm(SvmMode.AlwaysOff)]
        public void SubscriptionEvents()
        {
            // handled by middleware
            // this acts as a marker for the route listing
        }
    }
}