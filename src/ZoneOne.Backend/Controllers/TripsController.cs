using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ZoneOne.Backend.Extensions;
using ZoneOne.Backend.Middleware;
using ZoneOne.Backend.ModelFactories;
using ZoneOne.Backend.Models;
using ZoneOne.Static.Database;

namespace ZoneOne.Backend.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Produces("application/json")]
    [ProducesErrorResponseType(typeof(ISvm<HttpError>))]
    public class TripsController : Controller
    {
        private readonly StaticDbContext _db;
        private readonly TripResultFactory _tripFactory;
        public TripsController(StaticDbContext db, TripResultFactory tripFactory)
        {
            _db = db;
            _tripFactory = tripFactory;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetTrip(string id)
        {
            try
            {
                var guid = id.FromBase32();
                if (!await _db.Trips.AsQueryable().AnyAsync(v => v.Id == guid)) return NotFound();
                return Ok(await _tripFactory.GetTrip(guid));
            }
            catch (InvalidOperationException)
            {
                return HttpError.InvalidGuid(this, nameof(id));
            }
        }
    }
}