using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Routing;
using ZoneOne.Backend.Middleware;
using ZoneOne.Backend.Models;
using ZoneOne.Utilities;

namespace ZoneOne.Backend.Controllers
{
    [ApiController]
    [Route(".well-known")]
    [Produces("application/json")]
    [ProducesErrorResponseType(typeof(ISvm<HttpError>))]
    public class WellKnownController : Controller
    {
        private static string GetRouteName(IDictionary<string, string> values)
        {
            var controllerName = values["controller"];
            var actionName = values["action"];
            return $"{controllerName}:{actionName}";
        }
        
        private readonly IActionDescriptorCollectionProvider _actionDescriptorCollectionProvider;
        
        public WellKnownController(IActionDescriptorCollectionProvider actionDescriptorCollectionProvider)
        {
            _actionDescriptorCollectionProvider = actionDescriptorCollectionProvider;
        }
        
        /// <summary>
        /// Returns a list of every route
        /// </summary>
        [HttpGet("routes")]
        [ProducesResponseType(typeof(ISvm<ApiRoute[]>), 200)]
        public IActionResult GetRoutes()
        {
            var result =
                _actionDescriptorCollectionProvider.ActionDescriptors.Items
                    .Where(el => el.AttributeRouteInfo != null)
                    .Select(v =>
                    {
                        var authAttrs = v.EndpointMetadata
                            .Select(attr => attr as AuthorizeAttribute)
                            .WhereNotNull()
                            .ToArray();

                        var scopes = authAttrs
                            .Select(attr => attr.Policy)
                            .WhereNotNull();

                        var methodMetadata =
                            (HttpMethodMetadata) v.EndpointMetadata.Single(attr => attr is HttpMethodMetadata);
                        var method = methodMetadata.HttpMethods[0];

                        var name = GetRouteName(v.RouteValues);

                        return new ApiRoute
                        {
                            Name = name,
                            Method = method,
                            Template = v.AttributeRouteInfo.Template,
                            RequiredScopes = scopes,
                            RequiresAuth = authAttrs.Length > 0
                        };
                    });

            return Ok(result);
        }
    }
}