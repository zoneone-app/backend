using System;
using System.Linq;
using System.Numerics;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MoreLinq.Extensions;
using ProtoBuf;
using VectorTile;
using ZoneOne.Backend.Extensions;
using ZoneOne.Backend.MapBox;
using ZoneOne.Backend.Middleware;
using ZoneOne.Backend.ModelFactories;
using ZoneOne.Backend.Models;
using ZoneOne.Static.Data;
using ZoneOne.Static.Data.Util;
using ZoneOne.Static.Database;
using ZoneOne.Utilities;

namespace ZoneOne.Backend.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Produces("application/json")]
    [ProducesErrorResponseType(typeof(ISvm<HttpError>))]
    public class StopsController : Controller
    {
        private const float MinLon = 150;
        private const float MaxLon = 156;
        private const float MinLat = -29.5f;
        private const float MaxLat = -23.5f;

        private static double TileXToLong(int x, int z)
        {
            return x / (double) (1 << z) * 360.0 - 180;
        }

        private static double TileYToLat(int y, int z)
        {
            var n = Math.PI - 2.0 * Math.PI * y / (1 << z);
            return 180.0 / Math.PI * Math.Atan(0.5 * (Math.Exp(n) - Math.Exp(-n)));
        }

        private readonly StaticDbContext _db;
        private readonly TripResultFactory _tripFactory;

        public StopsController(StaticDbContext db, TripResultFactory tripFactory)
        {
            _db = db;
            _tripFactory = tripFactory;
        }

        /// <summary>
        /// Get a list of stops in the specified range
        /// </summary>
        /// <remarks>
        /// Errors for requests outside the range of lon=151 to 155 and lat=-28.5 to -24.5
        /// </remarks>
        /// <response code="400">Longitude or latitude out of range</response>
        [HttpGet]
        [ProducesResponseType(typeof(ISvm<BasicStop[]>), 200)]
        public IActionResult GetStops(float minLon, float minLat, float maxLon, float maxLat)
        {
            if (!minLon.Between(MinLon, MaxLon)) return BadRequest(new HttpError(nameof(minLon), HttpError.OutOfRange));
            if (!maxLon.Between(MinLon, MaxLon)) return BadRequest(new HttpError(nameof(maxLon), HttpError.OutOfRange));
            if (!minLat.Between(MinLat, MaxLat)) return BadRequest(new HttpError(nameof(minLat), HttpError.OutOfRange));
            if (!maxLat.Between(MinLat, MaxLat)) return BadRequest(new HttpError(nameof(maxLat), HttpError.OutOfRange));

            var result = _db.Stops.AsQueryable()
                .Where(v => v.Longitude > minLon && v.Longitude < maxLon &&
                            v.Latitude > minLat && v.Latitude < maxLat)
                .Select(v => new BasicStop(v.Id)
                {
                    Longitude = v.Longitude,
                    Latitude = v.Latitude
                });

            return Ok(result);
        }

        /// <summary>
        /// Gets a stop by its ID
        /// </summary>
        /// <response code="404">Stop does not exist</response>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(ISvm<StopResult>), 200)]
        public async Task<IActionResult> GetStop(string id)
        {
            var guid = id.FromBase32();

            if (!await AsyncEnumerable.AnyAsync(_db.Stops, v => v.Id == guid)) return NotFound();
            var stop = await _db.Stops.AsQueryable().FirstAsync(v => v.Id == guid);

            return Ok(new StopResult
            {
                Id = stop.Id.ToBase32(),
                GtfsId = stop.GtfsId,
                Name = stop.Name,
                StationId = stop.Station?.Id.ToBase32(),
                Suburb = stop.Suburb,
                Zone = stop.Zone,
                OtherZone = stop.OtherZone,
                IsDummy = stop.IsDummy
            });
        }

        /// <summary>
        /// Gets the trips visiting a stop
        /// </summary>
        /// <param name="id">Stop ID</param>
        /// <param name="day">The day to check, in days since epoch (2021/01/01)</param>
        /// <returns>Trips between start and end that visit this stop</returns>
        [HttpGet("{id}/trips")]
        [ProducesResponseType(typeof(ISvm<StopTripResult>), 200)]
        public async Task<IActionResult> GetStopTrips(string id, int day)
        {
            try
            {
                var guid = id.FromBase32();

                if (!await _db.Stops.AsQueryable().AnyAsync(it => it.Id == guid)) return NotFound();
                var stop = await _db.Stops.AsQueryable().FirstAsync(it => it.Id == guid);

                var weekDay = (day + DateUtil.EpochWeekday) % 7;
                var days = DateUtil.GetDay(weekDay);

                var extraServices =
                    from exception in _db.ServiceExceptions.AsQueryable()
                    where exception.Date == day
                    where exception.Type
                    select exception;

                var removedServices =
                    from exception in _db.ServiceExceptions.AsQueryable()
                    where exception.Date == day
                    where !exception.Type
                    select exception;

                var validServices =
                    from service in _db.Services.AsQueryable()
                    where service.Start <= day && service.End >= day
                    where (service.Days & days) != 0
                    where !removedServices.Any(ex => ex.ServiceId == service.Id) ||
                          extraServices.Any(ex => ex.ServiceId == service.Id)
                    select service.Id;

                var trips = await (
                    from timing in _db.Timings.AsQueryable()
                    join patternTiming in _db.PatternTimings on timing.PatternTimingId equals patternTiming.Id
                    join patternStop in _db.PatternStops on new
                            {timing.PositionInSequence, patternTiming.StoppingPatternId}
                        equals new
                        {
                            PositionInSequence = (int) patternStop.PositionInSequence, patternStop.StoppingPatternId
                        }
                    join trip in _db.Trips on new
                            {PatternTimingId = patternTiming.Id, patternStop.StoppingPatternId}
                        equals new {trip.PatternTimingId, trip.StoppingPatternId}
                    let arrivalTime = trip.StartTime + timing.TimeToStop
                    let route = trip.StoppingPattern.Route
                    join service in _db.Services on trip.ServiceId equals service.Id
                    where validServices.Any(v => v == service.Id)
                    let patternStopId = (
                        from stopOverride in route.PatternStopOverrides
                        where stopOverride.ExistingStopId == patternStop.StopId
                        select (Guid?) stopOverride.NewStopId
                    ).FirstOrDefault() ?? patternStop.StopId
                    where patternStopId == stop.Id
                    let stoppingPattern = trip.StoppingPattern
                    orderby arrivalTime
                    select new StopTrip
                    {
                        TripId = trip.Id.ToBase32(),
                        RouteId = stoppingPattern.RouteId.ToBase32(),
                        StoppingPatternId = stoppingPattern.Id.ToBase32(),

                        ArrivalTime = arrivalTime,
                        DwellDuration = timing.DwellDuration,

                        Days = service.Days
                    }
                ).ToArrayAsync();

                foreach (var trip in trips)
                {
                    trip.Trip = await _tripFactory.GetTrip(trip.TripId.FromBase32());
                }

                return Ok(new StopTripResult(trips.Count(), trips));
            }
            catch (InvalidOperationException)
            {
                return HttpError.InvalidGuid(this, nameof(id));
            }
        }

        /// <summary>
        /// Returns Mapbox JSON for the specified XYZ tile
        /// </summary>
        /// <remarks>Tile format is from https://wiki.openstreetmap.org/wiki/Slippy_map_tilenames</remarks>
        [HttpGet("mpbxvec/{z}/{y}/{x}.mvt")]
        [Produces("application/vnd.google.protobuf")]
        public void GetMpbxvec(int z, int y, int x)
        {
            var minLon = TileXToLong(x, z);
            var minLat = TileYToLat(y + 1, z);
            var maxLon = TileXToLong(x + 1, z);
            var maxLat = TileYToLat(y, z);

            var result = _db.Stops.AsQueryable()
                .Where(v => v.Longitude > minLon && v.Longitude < maxLon &&
                            v.Latitude > minLat && v.Latitude < maxLat)
                .Select(v => new BasicStop(v.Id)
                {
                    Longitude = v.Longitude,
                    Latitude = v.Latitude
                });

            var layer = new Tile.Layer
            {
                Version = 2,
                Name = "stops",
                Extent = 4096
            };

            layer.Keys.Add("id");

            result.ForEach((stop, index) =>
            {
                var pos = new Vector2(
                    stop.Longitude.Unmap((float) minLon, (float) maxLon),
                    stop.Latitude.Unmap((float) maxLat, (float) minLat)
                );

                var geom = new CommandBuilder()
                    .MoveTo((int) (pos.X * layer.Extent), (int) (pos.Y * layer.Extent))
                    .Build();

                layer.Features.Add(new Tile.Feature
                {
                    Id = (uint) index,
                    Type = Tile.GeomType.Point,
                    Geometries = geom,
                    Tags = new uint[] {0, (uint) index}
                });

                layer.Values.Add(new Tile.Value
                {
                    StringValue = stop.Id.ToBase32()
                });
            });

            var resultAsBuff = new Tile
            {
                Layers = {layer}
            };

            Response.ContentType = "application/vnd.mapbox-vector-tile";

            var stream = Response.Body;
            Serializer.Serialize(stream, resultAsBuff);
        }
    }
}