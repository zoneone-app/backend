namespace ZoneOne.Backend.Middleware
{
    /// <typeparam name="T">Result type</typeparam>
    public interface ISvm<out T>
    {
        /// <summary>
        /// The result of the operation
        /// </summary>
        public T? Value { get; }
        
        /// <summary>
        /// Any reasons the server cannot complete the request
        /// </summary>
        public string[] Status { get; }
    }
}