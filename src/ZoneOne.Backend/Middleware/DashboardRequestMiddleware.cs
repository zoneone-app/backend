using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using ZoneOne.Backend.Dashboard;
using ZoneOne.Backend.Models;

namespace ZoneOne.Backend.Middleware
{
    public class DashboardRequestMiddleware : IMiddleware
    {
        private readonly DashboardRt _rt;

        public DashboardRequestMiddleware(DashboardRt rt)
        {
            _rt = rt;
        }

        public async Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            var stopwatch = new Stopwatch();

            stopwatch.Start();
            await next(context);
            stopwatch.Stop();

            _rt.RequestChannel.Add(new RequestInfo(stopwatch.Elapsed.TotalMilliseconds));
        }
    }
}