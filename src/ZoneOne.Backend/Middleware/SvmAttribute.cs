using System;

namespace ZoneOne.Backend.Middleware
{
    public enum SvmMode
    {
        AlwaysOff,
        AlwaysOn,
        Normal
    }
    
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class SvmAttribute : Attribute
    {
        public SvmAttribute(SvmMode mode)
        {
            Mode = mode;
        }

        public SvmMode Mode { get; }
    }
}