using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Abstractions;
using Microsoft.AspNetCore.Routing;
using ZoneOne.Backend.BackgroundServices;

namespace ZoneOne.Backend.Middleware
{
    public class StateValueMiddleware : IMiddleware
    {
        private readonly ServiceStatus _serviceStatus;

        public StateValueMiddleware(ServiceStatus serviceStatus)
        {
            _serviceStatus = serviceStatus;
        }

        public async Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            // make sure we aren't interfering with Swagger
            if (context.Request.Path.StartsWithSegments("/swagger"))
            {
                await next(context);
                return;
            }

            var endpoint = context.Features.Get<IEndpointFeature>().Endpoint;
            var svmAttr = endpoint?.Metadata.GetMetadata<SvmAttribute>();

            var mode = svmAttr?.Mode ?? SvmMode.Normal;

            if (mode == SvmMode.AlwaysOff)
            {
                await next(context);
                return;
            }
            
            var currentStatus = new List<string>();

            if (mode == SvmMode.Normal)
            {
                if (_serviceStatus.Migration.IsRunning) currentStatus.Add("migrating");
                if (_serviceStatus.Importer.IsRunning) currentStatus.Add("importing");
            }

            if (currentStatus.Count > 0)
            {
                var res = new OkObjectResult(new
                {
                    Status = currentStatus,
                    Value = (string?) null
                });

                await res.ExecuteResultAsync(new ActionContext(context, context.GetRouteData(), new ActionDescriptor()));

                return;
            }

            await using var tempStream = new MemoryStream();
            var realBody = context.Response.Body;
            context.Response.Body = tempStream;
            
            await next(context);
            tempStream.Seek(0, SeekOrigin.Begin);

            context.Response.Body = realBody;

            // ignore any realtime requests (i.e. signalr) or requests that are not json
            if (!context.Request.Path.StartsWithSegments("/rt") &&
                (context.Response.ContentType?.StartsWith("application/json") ?? false))
            {
                // add the length of our wrapper
                if (context.Response.Headers.ContentLength.HasValue)
                    context.Response.Headers.ContentLength = 9 + 13 + context.Response.Headers.ContentLength;
                
                // wrap new json around it to make it fit with the schema
                await context.Response.WriteAsync("{\"value\":");
                await tempStream.CopyToAsync(context.Response.Body);
                await context.Response.WriteAsync(",\"status\":[]}");
            }
            else
            {
                await tempStream.CopyToAsync(context.Response.Body);
            }
        }
    }
}