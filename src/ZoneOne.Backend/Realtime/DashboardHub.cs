using System.Collections.Generic;
using Microsoft.AspNetCore.SignalR;
using ZoneOne.Backend.Dashboard;
using ZoneOne.Backend.Models;

namespace ZoneOne.Backend.Realtime
{
    public class DashboardHub : Hub
    {
        private readonly DashboardRt _rt;

        public DashboardHub(DashboardRt rt)
        {
            _rt = rt;
        }

        public IAsyncEnumerable<LogMessage> GetLogs() => _rt.LogChannel.Read();

        public RequestGraphItem GetRequestStatus() => _rt.RequestStatus;
    }
}