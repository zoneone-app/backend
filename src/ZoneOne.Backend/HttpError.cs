using Microsoft.AspNetCore.Mvc;

namespace ZoneOne.Backend
{
    public class HttpError
    {
        public const string OutOfRange = "Out of range";

        public static HttpError InvalidGuid(string param) => new(param, "Invalid ID specified");
        public static IActionResult InvalidGuid(ControllerBase ctrl, string param) =>
            ctrl.BadRequest(InvalidGuid(param));

        public bool Error => true;

        public string? Param { get; }

        public string Message { get; }

        public HttpError(string? param, string message)
        {
            Param = param;
            Message = message;
        }
    }
}