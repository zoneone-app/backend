using System;
using System.Collections.Concurrent;
using System.Threading.Channels;
using System.Threading.Tasks.Dataflow;
using Microsoft.Extensions.Logging;
using ZoneOne.Backend.Models;
using ZoneOne.Utilities;

namespace ZoneOne.Backend.Dashboard
{
    public class DashboardLogger : ILogger
    {
        private readonly string _name;
        private readonly MessageQueue<LogMessage> _target;

        public DashboardLogger(string name, MessageQueue<LogMessage> target)
        {
            _name = name;
            _target = target;
        }

        public IDisposable? BeginScope<TState>(TState state) => default;

        public bool IsEnabled(LogLevel logLevel) => true;

        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception,
            Func<TState, Exception, string> formatter)
        {
            var message = formatter(state, exception);
            _target.Add(new LogMessage(DateTime.UtcNow, eventId, logLevel, _name, message));
        }
    }
    
    public class DashboardLoggerProvider : ILoggerProvider
    {
        private readonly MessageQueue<LogMessage> _logChannel;
        private readonly ConcurrentDictionary<string, DashboardLogger> _loggers = new();

        public DashboardLoggerProvider(MessageQueue<LogMessage> logChannel)
        {
            _logChannel = logChannel;
        }

        public ILogger CreateLogger(string categoryName) =>
            _loggers.GetOrAdd(categoryName, name => new DashboardLogger(name, _logChannel));

        public void Dispose()
        {
            GC.SuppressFinalize(this);
            _loggers.Clear();
        }
    }
}