using System;
using System.Collections.Generic;
using System.Timers;
using ZoneOne.Backend.Models;
using ZoneOne.Utilities;

namespace ZoneOne.Backend.Dashboard
{
    public class DashboardRt
    {
        public MessageQueue<RequestInfo> RequestChannel { get; }
        public MessageQueue<LogMessage> LogChannel { get; }
        public DashboardLoggerProvider LoggerProvider { get; }

        public RequestGraphItem RequestStatus { get; private set; } = new(DateTime.UtcNow, 0, 0);

        public DashboardRt()
        {
            LogChannel = new MessageQueue<LogMessage>();
            LoggerProvider = new DashboardLoggerProvider(LogChannel);

            RequestChannel = new MessageQueue<RequestInfo>();
            BeginRequestReader(RequestChannel.Read());
            BeginRequestUpdater();
        }

        private void BeginRequestUpdater()
        {
            var timer = new Timer(1000) {AutoReset = true};
            timer.Start();

            timer.Elapsed += (_, _) =>
            {
                // clear the requests per second if it has been more than a second since the previous request
                if (RequestStatus.Time < DateTime.UtcNow - TimeSpan.FromSeconds(1))
                    RequestStatus = RequestStatus with {Time = DateTime.UtcNow, Count = 0};
            };
        }

        private async void BeginRequestReader(IAsyncEnumerable<RequestInfo> read)
        {
            var prevReqTime = DateTime.UtcNow;

            await foreach (var req in read)
            {
                var timeDiff = DateTime.UtcNow - prevReqTime;
                var perSec = TimeSpan.FromSeconds(1) / timeDiff;
                prevReqTime = DateTime.UtcNow;

                var avgDuration = (RequestStatus.MedDurationMs + req.DurationMs * 3) / 4;
                RequestStatus = new RequestGraphItem(DateTime.UtcNow, perSec, avgDuration);
            }
        }
    }
}