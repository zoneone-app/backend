using System;
using System.Collections.Generic;
using System.Linq;

namespace ZoneOne.Backend.MapBox
{
    public class CommandBuilder
    {
        private record CommandInfo(Command Command, IEnumerable<int> Parameters);

        private record CommandTracker(Command Command, List<uint> Parameters);

        private readonly List<CommandInfo> _commands = new();

        /// <summary>
        /// Adds a new command
        /// </summary>
        /// <returns>Same CommandBuilder instance for convenience</returns>
        public CommandBuilder Add(Command command, IEnumerable<int> args)
        {
            _commands.Add(new CommandInfo(command, args));
            return this;
        }

        public CommandBuilder MoveTo(int dx, int dy) => Add(Command.MoveTo, new[] { dx, dy });
        public CommandBuilder LineTo(int dx, int dy) => Add(Command.LineTo, new[] {dx, dy});
        public CommandBuilder ClosePath() => Add(Command.ClosePath, Array.Empty<int>());

        /// <summary>
        /// Builds the command
        /// </summary>
        /// <remarks>Groups adjacent MoveTo and LineTo commands to make result smaller</remarks>
        public uint[] Build()
        {
            var result = new List<uint>();

            var previousCommandInfo = default(CommandTracker?);
            foreach (var (command, args) in _commands)
            {
                if (previousCommandInfo?.Command != command)
                {
                    if (previousCommandInfo != null)
                    {
                        AddCommandInfo(result, previousCommandInfo);
                    }
                    
                    previousCommandInfo = new CommandTracker(command, new List<uint>());
                }

                previousCommandInfo.Parameters.AddRange(args.Select(CommandUtil.Parameter));
            }

            if (previousCommandInfo != null) AddCommandInfo(result, previousCommandInfo);
            
            return result.ToArray();
        }

        private static void AddCommandInfo(List<uint> result, CommandTracker previousCommandInfo)
        {
            var count = (uint) previousCommandInfo.Parameters.Count /
                        CommandUtil.GetParameterCount(previousCommandInfo.Command);
                        
            var commInt = CommandUtil.Integer(previousCommandInfo.Command, count);
                        
            result.Add(commInt);
            result.AddRange(previousCommandInfo.Parameters);
        }
    }
}