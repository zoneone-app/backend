using System;

namespace ZoneOne.Backend.MapBox
{
    public static class CommandUtil
    {
        /// <summary>
        /// Gets command integer based on command and count. See
        /// <a href="https://github.com/mapbox/vector-tile-spec/tree/master/2.1#431-command-integers">spec</a>.
        /// </summary>
        /// <param name="command">Command ID</param>
        /// <param name="count">Usage count</param>
        /// <returns>CommandInteger</returns>
        public static uint Integer(Command command, uint count) => ((uint) command & 0x07) | (count << 3);
        
        /// <summary>
        /// Gets parameter integer. See
        /// <a href="https://github.com/mapbox/vector-tile-spec/tree/master/2.1#432-parameter-integers">spec</a>.
        /// </summary>
        /// <param name="value">Parameter value</param>
        /// <returns>ParameterInteger</returns>
        public static uint Parameter(int value) => (uint) ((value << 1) ^ (value >> 31));

        /// <summary>
        /// Returns the amount of parameters the command takes
        /// </summary>
        public static uint GetParameterCount(Command command) =>
            command switch
            {
                Command.MoveTo => 2,
                Command.LineTo => 2,
                Command.ClosePath => 0,
                _ => throw new ArgumentOutOfRangeException(nameof(command), command, null)
            };
    }
}