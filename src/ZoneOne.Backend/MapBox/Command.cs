namespace ZoneOne.Backend.MapBox
{
    public enum Command
    {
        MoveTo = 1,
        LineTo = 2,
        ClosePath = 7
    }
}