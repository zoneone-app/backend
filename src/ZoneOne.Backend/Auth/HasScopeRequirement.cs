using Microsoft.AspNetCore.Authorization;

namespace ZoneOne.Backend.Auth
{
    public record HasScopeRequirement(string Scope, string Issuer) : IAuthorizationRequirement;
}