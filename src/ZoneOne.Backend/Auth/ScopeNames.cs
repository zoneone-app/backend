namespace ZoneOne.Backend.Auth
{
    public static class ScopeNames
    {
        public const string CreateDataImport = "create:data-import";
    }
}