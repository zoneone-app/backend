using Microsoft.AspNetCore.Authorization;

namespace ZoneOne.Backend.Auth
{
    public static class PolicyExtensions
    {
        public static void AddScopedPolicy(this AuthorizationOptions options, string scope)
        {
            options.AddPolicy(scope, policy =>
            {
                policy.AddRequirements(new HasScopeRequirement(scope, "https://zoneone.au.auth0.com/")).Build();
            });
        }
    }
}