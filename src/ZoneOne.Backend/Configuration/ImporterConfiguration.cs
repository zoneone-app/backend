using Microsoft.Extensions.Configuration;

namespace ZoneOne.Backend.Configuration
{
    public class ImporterConfiguration
    {
        private readonly IConfigurationSection _root;
        
        public ImporterConfiguration(IConfiguration configuration)
        {
            _root = configuration.GetSection("Importer");
        }

        public bool Enabled => _root.GetValue<bool>("Enabled");
        public string Source => _root.GetValue<string>("Source");
        public string StateFile => _root.GetValue<string>("StateFile");
    }
}