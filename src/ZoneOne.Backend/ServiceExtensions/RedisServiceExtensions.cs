using System.Linq;
using System.Net;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using StackExchange.Redis;

namespace ZoneOne.Backend.ServiceExtensions
{
    public static class RedisServiceExtensions
    {
        public static IServiceCollection AddRedis(this IServiceCollection services, IConfiguration configuration)
        {
            return services.AddScoped<IDatabase>(_ =>
            {
                var config = new ConfigurationOptions();

                config.EndPoints.Add(configuration.GetValue<string>("EndPoint"));
                
                return ConnectionMultiplexer.Connect(config).GetDatabase();
            });
        }
    }
}