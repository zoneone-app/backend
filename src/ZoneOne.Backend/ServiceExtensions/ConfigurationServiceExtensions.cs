using System;
using Microsoft.Extensions.DependencyInjection;
using ZoneOne.Backend.Configuration;

namespace ZoneOne.Backend.ServiceExtensions
{
    public static class ConfigurationServiceExtensions
    {
        public static void AddConfiguration(this IServiceCollection services)
        {
            services.AddTransient<ImporterConfiguration>();
        }
    }
}