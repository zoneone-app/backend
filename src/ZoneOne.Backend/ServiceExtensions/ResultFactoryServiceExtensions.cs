using Microsoft.Extensions.DependencyInjection;
using ZoneOne.Backend.ModelFactories;

namespace ZoneOne.Backend.ServiceExtensions
{
    public static class ResultFactoryServiceExtensions
    {
        public static IServiceCollection AddTripResultFactory(this IServiceCollection services) =>
            services.AddTransient<TripResultFactory>();

        public static IServiceCollection AddResultFactories(this IServiceCollection services) =>
            services
                .AddTripResultFactory();
    }
}