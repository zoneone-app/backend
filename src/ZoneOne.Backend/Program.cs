using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using ZoneOne.Backend.Dashboard;

namespace ZoneOne.Backend
{
    public class Program
    {
        private static IEnumerable<string?> ListFiles(string path)
        {
            if (Directory.Exists(path))
                foreach (var file in Directory.GetFiles(path, "*.json"))
                    yield return file;

            if (File.Exists(path)) yield return path;
            yield return null;
        }

        private static void LoadConfiguration(IConfigurationBuilder builder)
        {
            var path = Environment.GetEnvironmentVariable("CONFIG_PATH") ?? "settings.json";
            var paths = path.Split(';')
                .Select(ListFiles)
                .SelectMany(v => v)
                .Where(v => !string.IsNullOrWhiteSpace(v));
            
            foreach (var p in paths)
            {
                builder.AddJsonFile(p, true, true);
            }
        }

        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args)
        {
            var dashboardRt = new DashboardRt();
            
            return Host.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration(LoadConfiguration)
                .ConfigureLogging((ctx, b) =>
                {
                    if (ctx.HostingEnvironment.IsDevelopment()) b.AddDebug().AddConsole();
                    else b.AddDebug().AddJsonConsole();

                    b.AddProvider(dashboardRt.LoggerProvider);
                })
                .ConfigureServices(svc =>
                {
                    svc.AddSingleton(dashboardRt);
                })
                .ConfigureWebHostDefaults(webBuilder => { webBuilder.UseStartup<Startup>(); });
        }
    }
}