namespace ZoneOne.Static.Database
{
    public record DatabaseConnection
    {
        public string Username { get; init; }
        public string Password { get; init; }
        public string Address { get; init; }
        public int Port { get; init; }
        public string Database { get; init; }
        public string SslMode { get; init; }
    }
}