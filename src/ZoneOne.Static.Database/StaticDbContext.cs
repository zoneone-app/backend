using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Npgsql;
using ZoneOne.Static.Data;
using ZoneOne.Static.Data.Util;

namespace ZoneOne.Static.Database
{
    public class StaticDbContext : DbContext
    {
        private readonly IConfiguration _configuration;
        private readonly ILoggerFactory _loggerFactory;

        public override int SaveChanges(bool acceptAllChangesOnSuccess)
        {
            OnBeforeSaving();
            return base.SaveChanges(acceptAllChangesOnSuccess);
        }

        public override async Task<int> SaveChangesAsync(
            bool acceptAllChangesOnSuccess,
            CancellationToken cancellationToken = default(CancellationToken)
        )
        {
            OnBeforeSaving();
            return (await base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken));
        }

        private void OnBeforeSaving()
        {
            var entries = ChangeTracker.Entries();
            var utcNow = DateTime.UtcNow;

            foreach (var entry in entries)
            {
                // for entities that inherit from BaseEntity,
                // set UpdatedOn / CreatedOn appropriately
                if (entry.Entity is IDatedEntity trackable)
                {
                    switch (entry.State)
                    {
                        case EntityState.Modified:
                            // set the updated date to "now"
                            trackable.LastUpdated = utcNow;
                            // mark property as "don't touch"
                            // we don't want to update on a Modify operation
                            entry.Property(nameof(trackable.Created)).IsModified = false;
                            break;
                        
                        case EntityState.Added:
                            // set both updated and created date to "now"
                            trackable.Created = utcNow;
                            trackable.LastUpdated = utcNow;
                            break;
                    }
                }
            }
        }
        
        public StaticDbContext(IConfiguration configuration, ILoggerFactory loggerFactory)
        {
            _configuration = configuration;
            _loggerFactory = loggerFactory;
        }
        
        public DbSet<StoppingPattern> StoppingPatterns { get; init; }
        public DbSet<Station> Stations { get; init; }
        public DbSet<Stop> Stops { get; init; }
        public DbSet<Route> Routes { get; set; }
        public DbSet<RoutePair> RoutePairs { get; init; }
        public DbSet<ContinuingRoute> ContinuingRoutes { get; init; }
        public DbSet<TimingPoint> TimingPoints { get; init; }
        public DbSet<PatternTiming> PatternTimings { get; set; }
        public DbSet<PatternStop> PatternStops { get; set; }
        public DbSet<PatternStopOverrides> PatternStopOverrides { get; set; }
        public DbSet<SegmentStop> SegmentStops { get; set; }
        public DbSet<Service> Services { get; set; }
        public DbSet<ServiceException> ServiceExceptions { get; set; }
        public DbSet<Timing> Timings { get; set; }
        public DbSet<Trip> Trips { get; set; }
        public DbSet<HeadwayTrip> HeadwayTrips { get; set; }
        public DbSet<StationStopsRelation> StationStopsRelations { get; set; }
        public DbSet<ReplacementStop> ReplacementStops { get; set; }
        public DbSet<Operator> Operators { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder opts)
        {
            var connection = _configuration.GetSection("StaticDatabase").Get<DatabaseConnection>();

            opts.UseLoggerFactory(_loggerFactory);
            opts.UseLazyLoadingProxies();

            var csBuilder = new NpgsqlConnectionStringBuilder
            {
                Database = connection.Database,
                Host = connection.Address,
                Username = connection.Username,
                Password = connection.Password,
                Port = connection.Port,
                TrustServerCertificate = true,
                SslMode = connection.SslMode switch
                {
                    "require" => SslMode.Require,
                    "disable" => SslMode.Disable,
                    _ => SslMode.Prefer
                },
                IncludeErrorDetails = true
            };

            opts.UseNpgsql(csBuilder.ConnectionString);
        }

        static StaticDbContext()
        {
            NpgsqlConnection.GlobalTypeMapper.MapEnum<Route.RouteBranding>();
            NpgsqlConnection.GlobalTypeMapper.MapEnum<Route.RouteDirection>();
            NpgsqlConnection.GlobalTypeMapper.MapEnum<Route.RouteDirectionType>();
            NpgsqlConnection.GlobalTypeMapper.MapEnum<Route.RouteMode>();
            NpgsqlConnection.GlobalTypeMapper.MapEnum<Route.RouteStoppingPatternType>();
            NpgsqlConnection.GlobalTypeMapper.MapEnum<Route.RouteStyling>();
            
            NpgsqlConnection.GlobalTypeMapper.MapEnum<StylingType>();
            NpgsqlConnection.GlobalTypeMapper.MapEnum<StylingFlag>();
            NpgsqlConnection.GlobalTypeMapper.MapEnum<Zone>();
            
            NpgsqlConnection.GlobalTypeMapper.MapEnum<Station.LogicType>();
            
            NpgsqlConnection.GlobalTypeMapper.MapEnum<StoppingPattern.ExtendedVariation>();
            NpgsqlConnection.GlobalTypeMapper.MapEnum<StoppingPattern.Variation>();
            NpgsqlConnection.GlobalTypeMapper.MapEnum<StoppingPattern.Dir>();

            NpgsqlConnection.GlobalTypeMapper.MapEnum<StationStopsRelation.StationStopRelations>();
        }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Add uuid generation
            modelBuilder.HasPostgresExtension("uuid-ossp");
            
            // Add Enums
            modelBuilder.HasPostgresEnum<Route.RouteBranding>();
            modelBuilder.HasPostgresEnum<Route.RouteDirection>();
            modelBuilder.HasPostgresEnum<Route.RouteDirectionType>();
            modelBuilder.HasPostgresEnum<Route.RouteMode>();
            modelBuilder.HasPostgresEnum<Route.RouteStoppingPatternType>();
            modelBuilder.HasPostgresEnum<Route.RouteStyling>();
            
            modelBuilder.HasPostgresEnum<StylingType>();
            modelBuilder.HasPostgresEnum<StylingFlag>();
            modelBuilder.HasPostgresEnum<Zone>();
            
            modelBuilder.HasPostgresEnum<Station.LogicType>();
            
            modelBuilder.HasPostgresEnum<StoppingPattern.ExtendedVariation>();
            modelBuilder.HasPostgresEnum<StoppingPattern.Variation>();
            modelBuilder.HasPostgresEnum<StoppingPattern.Dir>();

            modelBuilder.HasPostgresEnum<StationStopsRelation.StationStopRelations>();
            
            BuildStoppingPattern(modelBuilder);
        }

        private void BuildStoppingPattern(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<StoppingPattern>(builder =>
            {
                builder.HasKey(v => v.Id);

                builder.Property(v => v.Direction);
                builder.Property(v => v.StartVariation);
                builder.Property(v => v.MiddleVariation);
                builder.Property(v => v.EndVariation);
                builder.Property(v => v.StoppingPatternLabel).HasMaxLength(40);
                builder.Property(v => v.MiddleVariationLabel).HasMaxLength(26);

                builder.HasOne(sp => sp.Route)
                    .WithMany(r => r.StoppingPatterns);
            });

            modelBuilder.Entity<Station>(builder =>
            {
                builder.HasKey(v => v.Id);

                builder.Property(v => v.Name).HasMaxLength(64);
                builder.Property(v => v.TypeLabel).HasMaxLength(32);
                builder.Property(v => v.Latitude);
                builder.Property(v => v.Longitude);
                builder.Property(v => v.StationType);
                builder.Property(v => v.TypeStyle);
                builder.Property(v => v.FlagStyle);
                builder.Property(v => v.ChildLabel).HasMaxLength(32);
                builder.Property(v => v.ChildLabelDescription).HasMaxLength(32);
                builder.Property(v => v.Desto).HasMaxLength(32);

                builder.HasMany(stn => stn.ChildrenStations)
                    .WithOne(stn => stn.StationGroup)
                    .IsRequired(false);
            });

            modelBuilder.Entity<Stop>(builder =>
            {
                builder.HasKey(v => v.Id);

                builder.Property(v => v.GtfsId);

                builder.Property(v => v.Name).HasMaxLength(64);
                
                builder.Property(v => v.Longitude);
                builder.Property(v => v.Latitude);

                builder.Property(v => v.Suburb).HasMaxLength(32);

                builder.Property(v => v.Zone);
                builder.Property(v => v.OtherZone);
                
                builder.Property(v => v.TypeStyle);
                builder.Property(v => v.FlagStyle);

                builder.Property(v => v.IsDummy).HasDefaultValue(false);;
                builder.Property(v => v.HideId).HasDefaultValue(false);

                builder.Property(v => v.Suburb).HasMaxLength(32);
                builder.Property(v => v.ChildLabel).HasMaxLength(32);
                builder.Property(v => v.ChildLabelDescription).HasMaxLength(32);

                builder.HasOne(s => s.Station)
                    .WithMany(stn => stn.ChildrenStops)
                    .IsRequired(false);
                builder.HasOne(s => s.MergeIntoStop)
                    .WithMany(s => s.MergedStops)
                    .IsRequired(false);
            });
            
            modelBuilder.Entity<Route>(builder =>
            {
                builder.HasKey(v => v.Id);

                builder.Property(v => v.OperatorId);
                builder.Property(v => v.Code).HasMaxLength(4);
                builder.Property(v => v.Title).HasMaxLength(42);
                builder.Property(v => v.ShortTitle).HasMaxLength(26);
                builder.Property(v => v.Description).HasMaxLength(256);
                builder.Property(v => v.Mode);
                builder.Property(v => v.Styling);
                builder.Property(v => v.DirectionType).HasDefaultValue(Route.RouteDirectionType.InOut);
                builder.Property(v => v.Direction).HasDefaultValue(Route.RouteDirection.TwoWay);
                builder.Property(v => v.Branding).HasDefaultValue(Route.RouteBranding.None);
                builder.Property(v => v.StoppingPatternType).HasDefaultValue(Route.RouteStoppingPatternType.AllStops);
                builder.Property(v => v.HailRide).HasDefaultValue(false);
                builder.Property(v => v.Free).HasDefaultValue(false);
                builder.Property(v => v.School).HasDefaultValue(false);
                builder.Property(v => v.AlwaysStop).HasDefaultValue(false);

                builder.HasOne(r => r.Operator)
                    .WithMany(o => o.Route)
                    .HasForeignKey(r => r.OperatorId);
            });
            
            modelBuilder.Entity<TimingPoint>(builder =>
            {
                builder.HasKey(v => new {v.RouteId, v.StopId});
                
                builder.Property(v => v.IsTimingPoint).HasDefaultValue(true);

                builder.HasOne(tp => tp.Route)
                    .WithMany(r => r.TimingPoints);

                builder.HasOne(tp => tp.Stop)
                    .WithMany(s => s.TimingPoints);
            });

            modelBuilder.Entity<RoutePair>(builder =>
            {
                builder.HasKey(v => new {v.AId, v.BId});
                builder.Property(v => v.Name).HasMaxLength(42);

                builder.HasOne(rp => rp.A)
                    .WithOne(r => r.PairA);
                builder.HasOne(rp => rp.B)
                    .WithOne(r => r.PairB);
            });

            modelBuilder.Entity<PatternTiming>(builder =>
            {
                builder.HasKey(v => v.Id);

                builder.HasOne(pt => pt.StoppingPattern)
                    .WithMany(sp => sp.PatternTimings);
            });

            modelBuilder.Entity<PatternStop>(builder =>
            {
                builder.HasKey(v => new {v.StoppingPatternId, v.PositionInSequence});
                builder.Property(v => v.NewDestination).HasDefaultValue(false);
                builder.Property(v => v.ContinuousDestination).HasDefaultValue(false);
                builder.Property(v => v.StopType)
                    .HasDefaultValue(PatternStop.StoppingType.Pickup | PatternStop.StoppingType.Pickup);
                
                builder.HasOne(ps => ps.StoppingPattern)
                    .WithMany(sp => sp.PatternStops);

                builder.HasOne(ps => ps.Stop)
                    .WithMany(s => s.PatternStops);
            });
            
            modelBuilder.Entity<PatternStopOverrides>(builder =>
            {
                builder.HasKey(v => new {v.RouteId, v.ExistingStopId});
                
                builder.HasOne(pso => pso.Route)
                    .WithMany(r => r.PatternStopOverrides);
                    

                builder.HasOne(pso => pso.ExistingStop)
                    .WithMany(s => s.ExistingPatternStops);
                
                builder.HasOne(pso => pso.NewStop)
                    .WithMany(s => s.NewPatternStops);
            });
            
            modelBuilder.Entity<SegmentStop>(builder =>
            {
                builder.HasKey(v => new { v.StartStopId, v.EndStopId });
                
                builder.Property(v => v.PathStr);
                builder.Property(v => v.PathStr2);
                builder.Property(v => v.PathStr3);

                builder.Ignore(v => v.Path);
                builder.Ignore(v => v.Path2);
                builder.Ignore(v => v.Path3);

                builder.HasOne(ss => ss.StartStop)
                    .WithMany(s => s.StartSegmentStops);

                builder.HasOne(ss => ss.EndStop)
                    .WithMany(s => s.EndSegmentStops);
            });

            modelBuilder.Entity<Service>(builder =>
            {
                builder.HasKey(v => v.Id);

                builder.Property(v => v.OperatorId);
                builder.Property(v => v.Days);
                builder.Property(v => v.Start);
                builder.Property(v => v.End);
                builder.Property(v => v.Period);
                builder.Property(v => v.Version);
                
                builder.HasOne(s => s.Operator)
                    .WithMany(o => o.Service);
            });

            modelBuilder.Entity<ServiceException>(builder =>
            {
                builder.HasKey(v => new { v.ServiceId, v.Date });;
                
                builder.HasOne(sve => sve.Service)
                    .WithMany(sv => sv.Exceptions);
            });

            modelBuilder.Entity<Timing>(builder =>
            {
                builder.HasKey(v => new { v.PatternTimingId, v.PositionInSequence });

                builder.Property(v => v.TimeToStop);
                builder.Property(v => v.DwellDuration);

                builder.HasOne(tm => tm.PatternTiming)
                    .WithMany(pt => pt.Timings);
            });

            modelBuilder.Entity<Trip>(builder =>
            {
                builder.HasKey(v => v.Id);
                builder.Property(v => v.GtfsTripId).HasMaxLength(48);
                builder.Property(v => v.StartTime);

                builder.HasOne(t => t.Service)
                    .WithMany(sv => sv.Trips);

                builder.HasOne(t => t.PatternTiming)
                    .WithMany(pt => pt.Trips);

                builder.HasOne(t => t.StoppingPattern)
                    .WithMany(sp => sp.Trips);
            });
            
            modelBuilder.Entity<HeadwayTrip>(builder =>
            {
                builder.HasKey(v => v.Id);
                builder.Property(v => v.GtfsTripId).HasMaxLength(48);
                builder.Property(v => v.StartTime);
                builder.Property(v => v.EndTime);
                builder.Property(v => v.Headway);
                
                builder.HasOne(t => t.Service)
                    .WithMany(sv => sv.HeadwayTrips);

                builder.HasOne(t => t.PatternTiming)
                    .WithMany(pt => pt.HeadwayTrips);

                builder.HasOne(t => t.StoppingPattern)
                    .WithMany(sp => sp.HeadwayTrips);
            });
            
            modelBuilder.Entity<ContinuingRoute>(builder =>
            {
                builder.HasKey(v => new { v.RouteInitialId, v.RouteFinalId, v.StopId });

                builder.HasOne(cr => cr.RouteInitial)
                    .WithMany(r => r.RouteInitials);
                
                builder.HasOne(cr => cr.RouteFinal)
                    .WithMany(r => r.RouteFinals);

                builder.HasOne(cr => cr.Stop)
                    .WithMany(s => s.ContinuingRoutes);
            });
            
            modelBuilder.Entity<ReplacementStop>(builder =>
            {
                builder.HasKey(v => new { v.StationId, v.StopId });

                builder.HasOne(rs => rs.Stop)
                    .WithMany(s => s.ReplacementStop);
                
                builder.HasOne(rs => rs.Station)
                    .WithMany(s => s.ReplacementStop);
            });

            modelBuilder.Entity<StationStopsRelation>(builder =>
            {
                builder.HasKey(v => new {v.StationId, v.StopAId, v.StopBId});

                builder.HasOne(ssr => ssr.Station)
                    .WithMany(s => s.StopRelationPair);

                builder.HasOne(ssr => ssr.StopA)
                    .WithMany(s => s.StopPairA);

                builder.HasOne(ssr => ssr.StopB)
                    .WithMany(s => s.StopPairB);
            });
            
            modelBuilder.Entity<Operator>(builder =>
            {
                builder.HasKey(v => v.Id);

                builder.Property(v => v.Code);
                builder.Property(v => v.Label);
                builder.Property(v => v.Prefix);

                builder.HasMany(o => o.Route)
                    .WithOne(r => r.Operator);
            });
        }
    }
}