﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using ZoneOne.Static.Data;
using ZoneOne.Static.Data.Util;

namespace ZoneOne.Static.Database.Migrations
{
    public partial class EnumChanges : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterDatabase()
                .Annotation("Npgsql:Enum:dir", "a,b,c")
                .Annotation("Npgsql:Enum:extended_variation", "normal,speedy_cat,skips_stops,extra_stops,alternate")
                .Annotation("Npgsql:Enum:logic_type", "interchange,kip,bayed,platformed,g_link,train,directional,ui_grouping")
                .Annotation("Npgsql:Enum:route_branding", "none,buz,city_glider,rocket,city_precincts,bullet,night_link,station_link")
                .Annotation("Npgsql:Enum:route_direction", "two_way,inv_two_way,primary,secondary,segments")
                .Annotation("Npgsql:Enum:route_direction_type", "in_out,north_south,east_west,clock,stream")
                .Annotation("Npgsql:Enum:route_mode", "bus,train,citycat,city_hopper,city_ferry,ferry,g_link,rail_bus")
                .Annotation("Npgsql:Enum:route_stopping_pattern_type", "all_stops,local,local_express,limited_stops,city_xpress,busway,limited_express,express")
                .Annotation("Npgsql:Enum:route_styling", "bus_express,bus_standard,city_glider_blue,city_glider_maroon,city_hopper,city_loop,ferry,glink,gold_coast_express,mt_coottha,night_link,spring_hill,tfb_buz,tfb_express,tfb_standard,train,train_dark_blue,train_green,train_grey,train_light_blue,train_purple,train_red,train_yellow,station_link,gu_shuttle,bac,qut")
                .Annotation("Npgsql:Enum:station_stop_relations", "island")
                .Annotation("Npgsql:Enum:styling_flag", "none,city_glider_blue,city_glider_maroon,bus_the_loop,bus_spring_hill,bus_night_link,bus_mt_coottha,rail_bus,bus_gc_express,gold_coast_express,ferry_city_hopper,busway,station_link,gu_shuttle,bac,qut")
                .Annotation("Npgsql:Enum:styling_type", "bcc_express,bcc_standard,bus_white,bus_black,bus_standard,bus_express,ferry_standard,ferry_white,g_link,train,bus_station,g_link_white,g_link_bus,group_orange,group_green,bcc_minor")
                .Annotation("Npgsql:Enum:variation", "normal,short,extra,alternate")
                .Annotation("Npgsql:Enum:zone", "airport,zone1,zone2,zone3,zone4,zone5,zone6,zone7,zone8")
                .Annotation("Npgsql:PostgresExtension:uuid-ossp", ",,");

            migrationBuilder.CreateTable(
                name: "Operators",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Code = table.Column<string>(type: "text", nullable: false),
                    Label = table.Column<string>(type: "text", nullable: true),
                    Prefix = table.Column<string>(type: "text", nullable: true),
                    Created = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    LastUpdated = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Operators", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Stations",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    TypeLabel = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    Longitude = table.Column<float>(type: "real", nullable: false),
                    Latitude = table.Column<float>(type: "real", nullable: false),
                    StationType = table.Column<Station.LogicType>(type: "logic_type", nullable: false),
                    TypeStyle = table.Column<StylingType>(type: "styling_type", nullable: false),
                    FlagStyle = table.Column<StylingFlag>(type: "styling_flag", nullable: false),
                    Desto = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    ChildLabel = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    ChildLabelDescription = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    StationGroupId = table.Column<Guid>(type: "uuid", nullable: true),
                    Created = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    LastUpdated = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Stations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Stations_Stations_StationGroupId",
                        column: x => x.StationGroupId,
                        principalTable: "Stations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Routes",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    OperatorId = table.Column<Guid>(type: "uuid", nullable: false),
                    Code = table.Column<string>(type: "character varying(4)", maxLength: 4, nullable: false),
                    Title = table.Column<string>(type: "character varying(42)", maxLength: 42, nullable: false),
                    Description = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: false),
                    Mode = table.Column<Route.RouteMode>(type: "route_mode", nullable: false),
                    Styling = table.Column<Route.RouteStyling>(type: "route_styling", nullable: false),
                    AlwaysStop = table.Column<bool>(type: "boolean", nullable: false, defaultValue: false),
                    ShortTitle = table.Column<string>(type: "character varying(26)", maxLength: 26, nullable: true),
                    DirectionType = table.Column<Route.RouteDirectionType>(type: "route_direction_type", nullable: true, defaultValue: Route.RouteDirectionType.InOut),
                    Direction = table.Column<Route.RouteDirection>(type: "route_direction", nullable: true, defaultValue: Route.RouteDirection.TwoWay),
                    Branding = table.Column<Route.RouteBranding>(type: "route_branding", nullable: true, defaultValue: Route.RouteBranding.None),
                    StoppingPatternType = table.Column<Route.RouteStoppingPatternType>(type: "route_stopping_pattern_type", nullable: true, defaultValue: Route.RouteStoppingPatternType.AllStops),
                    HailRide = table.Column<bool>(type: "boolean", nullable: true, defaultValue: false),
                    Free = table.Column<bool>(type: "boolean", nullable: true, defaultValue: false),
                    School = table.Column<bool>(type: "boolean", nullable: true, defaultValue: false),
                    Created = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    LastUpdated = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Routes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Routes_Operators_OperatorId",
                        column: x => x.OperatorId,
                        principalTable: "Operators",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Services",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    OperatorId = table.Column<Guid>(type: "uuid", nullable: false),
                    Days = table.Column<int>(type: "integer", nullable: false),
                    Start = table.Column<int>(type: "integer", nullable: false),
                    End = table.Column<int>(type: "integer", nullable: false),
                    Period = table.Column<string>(type: "text", nullable: false),
                    Version = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Services", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Services_Operators_OperatorId",
                        column: x => x.OperatorId,
                        principalTable: "Operators",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Stops",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    GtfsId = table.Column<int>(type: "integer", nullable: false),
                    Name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    Longitude = table.Column<float>(type: "real", nullable: false),
                    Latitude = table.Column<float>(type: "real", nullable: false),
                    Suburb = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    Zone = table.Column<Zone>(type: "zone", nullable: false),
                    IsDummy = table.Column<bool>(type: "boolean", nullable: false, defaultValue: false),
                    ChildLabel = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    ChildLabelDescription = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    OtherZone = table.Column<Zone>(type: "zone", nullable: true),
                    TypeStyle = table.Column<StylingType>(type: "styling_type", nullable: true),
                    FlagStyle = table.Column<StylingFlag>(type: "styling_flag", nullable: true),
                    HideId = table.Column<bool>(type: "boolean", nullable: true, defaultValue: false),
                    MergeIntoStopId = table.Column<Guid>(type: "uuid", nullable: true),
                    StationId = table.Column<Guid>(type: "uuid", nullable: true),
                    Created = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    LastUpdated = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Stops", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Stops_Stations_StationId",
                        column: x => x.StationId,
                        principalTable: "Stations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Stops_Stops_MergeIntoStopId",
                        column: x => x.MergeIntoStopId,
                        principalTable: "Stops",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "RoutePairs",
                columns: table => new
                {
                    AId = table.Column<Guid>(type: "uuid", nullable: false),
                    BId = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "character varying(42)", maxLength: 42, nullable: false),
                    Created = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    LastUpdated = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RoutePairs", x => new { x.AId, x.BId });
                    table.ForeignKey(
                        name: "FK_RoutePairs_Routes_AId",
                        column: x => x.AId,
                        principalTable: "Routes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_RoutePairs_Routes_BId",
                        column: x => x.BId,
                        principalTable: "Routes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "StoppingPatterns",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    RouteId = table.Column<Guid>(type: "uuid", nullable: false),
                    Direction = table.Column<StoppingPattern.Dir>(type: "dir", nullable: false),
                    StartVariation = table.Column<StoppingPattern.Variation>(type: "variation", nullable: false),
                    MiddleVariation = table.Column<StoppingPattern.ExtendedVariation>(type: "extended_variation", nullable: false),
                    EndVariation = table.Column<StoppingPattern.Variation>(type: "variation", nullable: false),
                    StoppingPatternLabel = table.Column<string>(type: "character varying(40)", maxLength: 40, nullable: true),
                    MiddleVariationLabel = table.Column<string>(type: "character varying(26)", maxLength: 26, nullable: true),
                    Created = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    LastUpdated = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StoppingPatterns", x => x.Id);
                    table.ForeignKey(
                        name: "FK_StoppingPatterns_Routes_RouteId",
                        column: x => x.RouteId,
                        principalTable: "Routes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ServiceExceptions",
                columns: table => new
                {
                    ServiceId = table.Column<Guid>(type: "uuid", nullable: false),
                    Date = table.Column<int>(type: "integer", nullable: false),
                    Type = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ServiceExceptions", x => new { x.ServiceId, x.Date });
                    table.ForeignKey(
                        name: "FK_ServiceExceptions_Services_ServiceId",
                        column: x => x.ServiceId,
                        principalTable: "Services",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ContinuingRoutes",
                columns: table => new
                {
                    RouteInitialId = table.Column<Guid>(type: "uuid", nullable: false),
                    RouteFinalId = table.Column<Guid>(type: "uuid", nullable: false),
                    StopId = table.Column<Guid>(type: "uuid", nullable: false),
                    Created = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    LastUpdated = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContinuingRoutes", x => new { x.RouteInitialId, x.RouteFinalId, x.StopId });
                    table.ForeignKey(
                        name: "FK_ContinuingRoutes_Routes_RouteFinalId",
                        column: x => x.RouteFinalId,
                        principalTable: "Routes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ContinuingRoutes_Routes_RouteInitialId",
                        column: x => x.RouteInitialId,
                        principalTable: "Routes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ContinuingRoutes_Stops_StopId",
                        column: x => x.StopId,
                        principalTable: "Stops",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PatternStopOverrides",
                columns: table => new
                {
                    RouteId = table.Column<Guid>(type: "uuid", nullable: false),
                    ExistingStopId = table.Column<Guid>(type: "uuid", nullable: false),
                    NewStopId = table.Column<Guid>(type: "uuid", nullable: false),
                    Created = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    LastUpdated = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PatternStopOverrides", x => new { x.RouteId, x.ExistingStopId });
                    table.ForeignKey(
                        name: "FK_PatternStopOverrides_Routes_RouteId",
                        column: x => x.RouteId,
                        principalTable: "Routes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PatternStopOverrides_Stops_ExistingStopId",
                        column: x => x.ExistingStopId,
                        principalTable: "Stops",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PatternStopOverrides_Stops_NewStopId",
                        column: x => x.NewStopId,
                        principalTable: "Stops",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ReplacementStops",
                columns: table => new
                {
                    StationId = table.Column<Guid>(type: "uuid", nullable: false),
                    StopId = table.Column<Guid>(type: "uuid", nullable: false),
                    Label = table.Column<string>(type: "text", nullable: false),
                    Created = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    LastUpdated = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReplacementStops", x => new { x.StationId, x.StopId });
                    table.ForeignKey(
                        name: "FK_ReplacementStops_Stations_StationId",
                        column: x => x.StationId,
                        principalTable: "Stations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ReplacementStops_Stops_StopId",
                        column: x => x.StopId,
                        principalTable: "Stops",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SegmentStops",
                columns: table => new
                {
                    StartStopId = table.Column<Guid>(type: "uuid", nullable: false),
                    EndStopId = table.Column<Guid>(type: "uuid", nullable: false),
                    PathStr = table.Column<string>(type: "text", nullable: false),
                    PathStr2 = table.Column<string>(type: "text", nullable: true),
                    PathStr3 = table.Column<string>(type: "text", nullable: true),
                    Created = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    LastUpdated = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SegmentStops", x => new { x.StartStopId, x.EndStopId });
                    table.ForeignKey(
                        name: "FK_SegmentStops_Stops_EndStopId",
                        column: x => x.EndStopId,
                        principalTable: "Stops",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SegmentStops_Stops_StartStopId",
                        column: x => x.StartStopId,
                        principalTable: "Stops",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "StationStopsRelations",
                columns: table => new
                {
                    StationId = table.Column<Guid>(type: "uuid", nullable: false),
                    StopAId = table.Column<Guid>(type: "uuid", nullable: false),
                    StopBId = table.Column<Guid>(type: "uuid", nullable: false),
                    Relation = table.Column<StationStopsRelation.StationStopRelations>(type: "station_stop_relations", nullable: false),
                    Created = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    LastUpdated = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StationStopsRelations", x => new { x.StationId, x.StopAId, x.StopBId });
                    table.ForeignKey(
                        name: "FK_StationStopsRelations_Stations_StationId",
                        column: x => x.StationId,
                        principalTable: "Stations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_StationStopsRelations_Stops_StopAId",
                        column: x => x.StopAId,
                        principalTable: "Stops",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_StationStopsRelations_Stops_StopBId",
                        column: x => x.StopBId,
                        principalTable: "Stops",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TimingPoints",
                columns: table => new
                {
                    RouteId = table.Column<Guid>(type: "uuid", nullable: false),
                    StopId = table.Column<Guid>(type: "uuid", nullable: false),
                    IsTimingPoint = table.Column<bool>(type: "boolean", nullable: false, defaultValue: true),
                    Created = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    LastUpdated = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TimingPoints", x => new { x.RouteId, x.StopId });
                    table.ForeignKey(
                        name: "FK_TimingPoints_Routes_RouteId",
                        column: x => x.RouteId,
                        principalTable: "Routes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TimingPoints_Stops_StopId",
                        column: x => x.StopId,
                        principalTable: "Stops",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PatternStops",
                columns: table => new
                {
                    StoppingPatternId = table.Column<Guid>(type: "uuid", nullable: false),
                    PositionInSequence = table.Column<short>(type: "smallint", nullable: false),
                    StopId = table.Column<Guid>(type: "uuid", nullable: false),
                    StopType = table.Column<int>(type: "integer", nullable: false, defaultValue: 1),
                    NewDestination = table.Column<bool>(type: "boolean", nullable: true, defaultValue: false),
                    ContinuousDestination = table.Column<bool>(type: "boolean", nullable: true, defaultValue: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PatternStops", x => new { x.StoppingPatternId, x.PositionInSequence });
                    table.ForeignKey(
                        name: "FK_PatternStops_StoppingPatterns_StoppingPatternId",
                        column: x => x.StoppingPatternId,
                        principalTable: "StoppingPatterns",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PatternStops_Stops_StopId",
                        column: x => x.StopId,
                        principalTable: "Stops",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PatternTimings",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    StoppingPatternId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PatternTimings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PatternTimings_StoppingPatterns_StoppingPatternId",
                        column: x => x.StoppingPatternId,
                        principalTable: "StoppingPatterns",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "HeadwayTrips",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    GtfsTripId = table.Column<string>(type: "character varying(48)", maxLength: 48, nullable: false),
                    ServiceId = table.Column<Guid>(type: "uuid", nullable: false),
                    PatternTimingId = table.Column<Guid>(type: "uuid", nullable: false),
                    StoppingPatternId = table.Column<Guid>(type: "uuid", nullable: false),
                    StartTime = table.Column<short>(type: "smallint", nullable: false),
                    EndTime = table.Column<short>(type: "smallint", nullable: false),
                    Headway = table.Column<short>(type: "smallint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HeadwayTrips", x => x.Id);
                    table.ForeignKey(
                        name: "FK_HeadwayTrips_PatternTimings_PatternTimingId",
                        column: x => x.PatternTimingId,
                        principalTable: "PatternTimings",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_HeadwayTrips_Services_ServiceId",
                        column: x => x.ServiceId,
                        principalTable: "Services",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_HeadwayTrips_StoppingPatterns_StoppingPatternId",
                        column: x => x.StoppingPatternId,
                        principalTable: "StoppingPatterns",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Timings",
                columns: table => new
                {
                    PatternTimingId = table.Column<Guid>(type: "uuid", nullable: false),
                    PositionInSequence = table.Column<int>(type: "integer", nullable: false),
                    TimeToStop = table.Column<short>(type: "smallint", nullable: false),
                    DwellDuration = table.Column<short>(type: "smallint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Timings", x => new { x.PatternTimingId, x.PositionInSequence });
                    table.ForeignKey(
                        name: "FK_Timings_PatternTimings_PatternTimingId",
                        column: x => x.PatternTimingId,
                        principalTable: "PatternTimings",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Trips",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    GtfsTripId = table.Column<string>(type: "character varying(48)", maxLength: 48, nullable: false),
                    ServiceId = table.Column<Guid>(type: "uuid", nullable: false),
                    PatternTimingId = table.Column<Guid>(type: "uuid", nullable: false),
                    StoppingPatternId = table.Column<Guid>(type: "uuid", nullable: false),
                    StartTime = table.Column<short>(type: "smallint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Trips", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Trips_PatternTimings_PatternTimingId",
                        column: x => x.PatternTimingId,
                        principalTable: "PatternTimings",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Trips_Services_ServiceId",
                        column: x => x.ServiceId,
                        principalTable: "Services",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Trips_StoppingPatterns_StoppingPatternId",
                        column: x => x.StoppingPatternId,
                        principalTable: "StoppingPatterns",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ContinuingRoutes_RouteFinalId",
                table: "ContinuingRoutes",
                column: "RouteFinalId");

            migrationBuilder.CreateIndex(
                name: "IX_ContinuingRoutes_StopId",
                table: "ContinuingRoutes",
                column: "StopId");

            migrationBuilder.CreateIndex(
                name: "IX_HeadwayTrips_PatternTimingId",
                table: "HeadwayTrips",
                column: "PatternTimingId");

            migrationBuilder.CreateIndex(
                name: "IX_HeadwayTrips_ServiceId",
                table: "HeadwayTrips",
                column: "ServiceId");

            migrationBuilder.CreateIndex(
                name: "IX_HeadwayTrips_StoppingPatternId",
                table: "HeadwayTrips",
                column: "StoppingPatternId");

            migrationBuilder.CreateIndex(
                name: "IX_PatternStopOverrides_ExistingStopId",
                table: "PatternStopOverrides",
                column: "ExistingStopId");

            migrationBuilder.CreateIndex(
                name: "IX_PatternStopOverrides_NewStopId",
                table: "PatternStopOverrides",
                column: "NewStopId");

            migrationBuilder.CreateIndex(
                name: "IX_PatternStops_StopId",
                table: "PatternStops",
                column: "StopId");

            migrationBuilder.CreateIndex(
                name: "IX_PatternTimings_StoppingPatternId",
                table: "PatternTimings",
                column: "StoppingPatternId");

            migrationBuilder.CreateIndex(
                name: "IX_ReplacementStops_StopId",
                table: "ReplacementStops",
                column: "StopId");

            migrationBuilder.CreateIndex(
                name: "IX_RoutePairs_AId",
                table: "RoutePairs",
                column: "AId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_RoutePairs_BId",
                table: "RoutePairs",
                column: "BId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Routes_OperatorId",
                table: "Routes",
                column: "OperatorId");

            migrationBuilder.CreateIndex(
                name: "IX_SegmentStops_EndStopId",
                table: "SegmentStops",
                column: "EndStopId");

            migrationBuilder.CreateIndex(
                name: "IX_Services_OperatorId",
                table: "Services",
                column: "OperatorId");

            migrationBuilder.CreateIndex(
                name: "IX_Stations_StationGroupId",
                table: "Stations",
                column: "StationGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_StationStopsRelations_StopAId",
                table: "StationStopsRelations",
                column: "StopAId");

            migrationBuilder.CreateIndex(
                name: "IX_StationStopsRelations_StopBId",
                table: "StationStopsRelations",
                column: "StopBId");

            migrationBuilder.CreateIndex(
                name: "IX_StoppingPatterns_RouteId",
                table: "StoppingPatterns",
                column: "RouteId");

            migrationBuilder.CreateIndex(
                name: "IX_Stops_MergeIntoStopId",
                table: "Stops",
                column: "MergeIntoStopId");

            migrationBuilder.CreateIndex(
                name: "IX_Stops_StationId",
                table: "Stops",
                column: "StationId");

            migrationBuilder.CreateIndex(
                name: "IX_TimingPoints_StopId",
                table: "TimingPoints",
                column: "StopId");

            migrationBuilder.CreateIndex(
                name: "IX_Trips_PatternTimingId",
                table: "Trips",
                column: "PatternTimingId");

            migrationBuilder.CreateIndex(
                name: "IX_Trips_ServiceId",
                table: "Trips",
                column: "ServiceId");

            migrationBuilder.CreateIndex(
                name: "IX_Trips_StoppingPatternId",
                table: "Trips",
                column: "StoppingPatternId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ContinuingRoutes");

            migrationBuilder.DropTable(
                name: "HeadwayTrips");

            migrationBuilder.DropTable(
                name: "PatternStopOverrides");

            migrationBuilder.DropTable(
                name: "PatternStops");

            migrationBuilder.DropTable(
                name: "ReplacementStops");

            migrationBuilder.DropTable(
                name: "RoutePairs");

            migrationBuilder.DropTable(
                name: "SegmentStops");

            migrationBuilder.DropTable(
                name: "ServiceExceptions");

            migrationBuilder.DropTable(
                name: "StationStopsRelations");

            migrationBuilder.DropTable(
                name: "TimingPoints");

            migrationBuilder.DropTable(
                name: "Timings");

            migrationBuilder.DropTable(
                name: "Trips");

            migrationBuilder.DropTable(
                name: "Stops");

            migrationBuilder.DropTable(
                name: "PatternTimings");

            migrationBuilder.DropTable(
                name: "Services");

            migrationBuilder.DropTable(
                name: "Stations");

            migrationBuilder.DropTable(
                name: "StoppingPatterns");

            migrationBuilder.DropTable(
                name: "Routes");

            migrationBuilder.DropTable(
                name: "Operators");
        }
    }
}
