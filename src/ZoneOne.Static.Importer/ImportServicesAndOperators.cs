using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Query;
using Microsoft.EntityFrameworkCore.Query.SqlExpressions;
using Microsoft.Extensions.Logging;
using Npgsql.EntityFrameworkCore.PostgreSQL.Update.Internal;
using ZoneOne.Static.Data;
using ZoneOne.Static.Data.Util;
using ZoneOne.Static.Importer.ImporterRecords;
using ZoneOne.Utilities;

namespace ZoneOne.Static.Importer
{
    public partial class StaticDataImporter
    {
        private async Task<(IEnumerable<ServiceIdMapping>, IDictionary<string, ServiceFrequency>)>
            ImportServicesAndOperators(
                GtfsLoader gtfs)
        {
            _logger.LogInformation("Importing services & operators");

            var regex = new Regex("^(?<Operator>[A-Za-z0-9]+)[ _](?<Period>.+)-(?<Version>\\d{2})(?<DOW>-\\d{7})?$");
            
            var existingOperators = _db.Operators.AsQueryable()
                .Select(v => new {v.Code, v.Id})
                .ToDictionary(v => v.Code, v => v.Id);
            
            _logger.LogDebug("Collecting Services, Frequnecies and Operators");
            var (operatorsServicesFrequenciesList, _) = await gtfs.GetCalendars().AggregateSelect(
                (record, existingOperators) =>
                {
                    Operator operatorRecord = null;
                    ServiceWithGtfsId service;
                    ServiceFrequency frequenciesListInternal;

                    var serviceParts = regex.Match(record.ServiceId);
                    var operatorCode = serviceParts.Groups["Operator"].Value;
                    var newOperator = !existingOperators.ContainsKey(operatorCode);
                    var operatorId = newOperator ? Guid.NewGuid() : existingOperators[operatorCode];
                    if (newOperator)
                    {
                        operatorRecord = new Operator(
                            operatorId,
                            operatorCode
                        );
                        existingOperators[operatorCode] = operatorId;
                    }

                    service = new ServiceWithGtfsId(record.ServiceId, new Service(
                        Guid.NewGuid(),
                        operatorId,
                        (record.Monday == 0 ? 0 : Days.Monday) |
                        (record.Tuesday == 0 ? 0 : Days.Tuesday) |
                        (record.Wednesday == 0 ? 0 : Days.Wednesday) |
                        (record.Thursday == 0 ? 0 : Days.Thursday) |
                        (record.Friday == 0 ? 0 : Days.Friday) |
                        (record.Saturday == 0 ? 0 : Days.Saturday) |
                        (record.Sunday == 0 ? 0 : Days.Sunday),
                        DateUtil.GetEpochFromDate(record.StartDate),
                        DateUtil.GetEpochFromDate(record.EndDate),
                        serviceParts.Groups["Period"].Value.Replace(serviceParts.Groups["Operator"]
                                                                    + "_", "") + serviceParts.Groups["DOW"].Value,
                        Convert.ToInt32(serviceParts.Groups["Version"].Value)
                    ));

                    frequenciesListInternal = new ServiceFrequency(
                        record.ServiceId,
                        record.Monday + record.Tuesday + record.Wednesday + record.Thursday + record.Friday +
                        record.Saturday + record.Sunday
                    );

                    return ((operatorRecord, service, frequenciesListInternal), existingOperators);
                }, existingOperators);


            var (operators, services, frequenciesLists) = operatorsServicesFrequenciesList.Unzip();

            _logger.LogDebug("Inserting Operators");
            if (operators is not null) await _db.Operators.AddRangeAsync(operators.WhereNotNull());
            await _db.SaveChangesAsync();
            
            
            _logger.LogDebug("Inserting Services and Service Exceptions");
            var exceptions = await gtfs.GetCalendarExceptions().ToLookupAsync(v => v.ServiceId);
            var serviceMapping = new List<ServiceIdMapping>();

            foreach (var (id, service) in services)
            {
                _logger.LogTrace("Adding service {ServiceId}", id);

                await _db.Services.AddAsync(service);

                var exceptionList = exceptions[id]
                    .Select(v => new ServiceException(
                        service.Id,
                        DateUtil.GetEpochFromDate(v.Date),
                        v.ExceptionType == 1));

                await _db.ServiceExceptions.AddRangeAsync(exceptionList);
                serviceMapping.Add(new ServiceIdMapping(id, service.Id));
            }

            var frequencies = frequenciesLists.ToDictionary(v => v.ServiceId,
                v => new ServiceFrequency(v.ServiceId, v.Frequency));
            return (serviceMapping, frequencies);
        }

        private async Task<Static.Data.Operator?> GetDbOperator(string routeOperator)
        {
            return await _db.Operators.FirstOrDefaultAsync(v => v.Code == routeOperator) ??
                   throw new NullReferenceException($"No operator found with code {routeOperator}");
        }
    }
}