using System;

namespace ZoneOne.Static.Importer
{
    public interface IDirectorySource : IDisposable
    {
        public string Directory { get; }
    }
}