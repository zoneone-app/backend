using CsvHelper.Configuration.Attributes;

namespace ZoneOne.Static.Importer.Data
{
    public class StopTime
    {
        [Name("trip_id")] public string TripId { get; set; }
        [Name("arrival_time")] public string ArrivalTime { get; set; }
        [Name("departure_time")] public string DepartureTime { get; set; }
        [Name("stop_id")] public int StopId { get; set; }
        [Name("stop_sequence")] public int StopSequence { get; set; }
        [Name("drop_off_type")] public bool DropoffType { get; set; }
        [Name("pickup_type")] public bool PickupType { get; set; }
        [Name("timepoint")] public bool TimePoint { get; set; }
    }
}