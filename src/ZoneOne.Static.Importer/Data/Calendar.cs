using System;
using CsvHelper.Configuration.Attributes;

namespace ZoneOne.Static.Importer.Data
{
    public class Calendar
    {
        [Name("service_id")] public string ServiceId { get; set; }

        [Name("monday")] public int Monday { get; set; }

        [Name("tuesday")] public int Tuesday { get; set; }

        [Name("wednesday")] public int Wednesday { get; set; }

        [Name("thursday")] public int Thursday { get; set; }

        [Name("friday")] public int Friday { get; set; }

        [Name("saturday")] public int Saturday { get; set; }

        [Name("sunday")] public int Sunday { get; set; }

        [Name("start_date")]
        [Format("yyyyMMdd")]
        public DateTime StartDate { get; set; }

        [Name("end_date")]
        [Format("yyyyMMdd")]
        public DateTime EndDate { get; set; }
    }
}