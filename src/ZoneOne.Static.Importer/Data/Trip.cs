using CsvHelper.Configuration.Attributes;

namespace ZoneOne.Static.Importer.Data
{
    public class Trip
    {
        [Name("route_id")] public string RouteId { get; set; }

        [Name("service_id")] public string ServiceId { get; set; }

        [Name("trip_id")] public string TripId { get; set; }

        [Name("trip_headsign")] public string TripHeadsign { get; set; }

        [Name("direction_id")] public bool DirectionId { get; set; }

        [Name("shape_id")] public string ShapeId { get; set; }
    }
}