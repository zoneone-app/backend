using CsvHelper.Configuration.Attributes;

namespace ZoneOne.Static.Importer.Data
{
    public class Stop
    {
        [Name("stop_id")] public string Id { get; set; }

        [Name("stop_code")] public string Code { get; set; }

        [Name("stop_name")] public string Name { get; set; }

        [Name("stop_desc")] public string Description { get; set; }

        [Name("stop_lat")] public float Latitude { get; set; }

        [Name("stop_lon")] public float Longitude { get; set; }

        [Name("zone_id")] public string Zone { get; set; }

        [Name("stop_url")] public string Url { get; set; }

        [Name("location_type")] public int Type { get; set; }

        [Name("parent_station")] public string Parent { get; set; }

        [Name("platform_code")] public string Relation { get; set; }
    }
}