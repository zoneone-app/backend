using CsvHelper.Configuration.Attributes;

namespace ZoneOne.Static.Importer.Data
{
    public class Route
    {
        [Name("route_id")] public string Id { get; set; }

        [Name("route_short_name")] public string Code { get; set; }

        [Name("route_long_name")] public string Name { get; set; }

        [Name("route_desc")] public string Description { get; set; }

        [Name("route_type")] public int Mode { get; set; }

        [Name("route_url")] public string Url { get; set; }

        [Name("route_color")] public string RouteColor { get; set; }

        [Name("route_text_color")] public string RouteTextColour { get; set; }
    }
}