using System;
using CsvHelper.Configuration.Attributes;

namespace ZoneOne.Static.Importer.Data
{
    public class CalendarException
    {
        [Name("service_id")] public string ServiceId { get; set; }

        [Name("date")] [Format("yyyyMMdd")] public DateTime Date { get; set; }

        [Name("exception_type")] public byte ExceptionType { get; set; }
    }
}