using CsvHelper.Configuration.Attributes;

namespace ZoneOne.Static.Importer.Data
{
    public class Shape
    {
        [Name("shape_id")] public string Id { get; set; }

        [Name("shape_pt_lon")] public float Longitude { get; set; }

        [Name("shape_pt_lat")] public float Latitude { get; set; }

        [Name("shape_pt_sequence")] public string Sequence { get; set; }
    }
}