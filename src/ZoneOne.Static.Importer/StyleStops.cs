﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using ZoneOne.Static.Data;
using ZoneOne.Static.Data.Util;
using ZoneOne.Static.Importer.Data;
using ZoneOne.Utilities;
using Route = ZoneOne.Static.Data.Route;
using Stop = ZoneOne.Static.Data.Stop;

namespace ZoneOne.Static.Importer
{
    public partial class StaticDataImporter
    {
        private static readonly string[] CityHopperCodes =
        {
            "SYDS", "NTHQ"
        };
        
        private static readonly string[] GcExpressBuses =
        {
            "777", "TX7"
        };

        private const string CityGliderBlue = "60";
        private const string CityGliderMaroon = "61";
        private const string SpringHill = "30";
        private const string MtCoottha = "480";
        private const string StationLink = "109";

        private static readonly string[] CityLoop =
        {
            "40", "50"
        };

        
        private async Task StyleStops()
        {
            Console.WriteLine("Starting to Style our stops");
            var stopRoutesUngrouped =
                (from route in _db.Routes.AsQueryable()
                join stoppingPattern in _db.StoppingPatterns on route equals stoppingPattern.Route
                join trip in _db.Trips on stoppingPattern equals trip.StoppingPattern
                join patternStop in _db.PatternStops on stoppingPattern equals patternStop.StoppingPattern
                join operatorRecord in _db.Operators on route.OperatorId equals operatorRecord.Id
                where patternStop.Stop.FlagStyle == null || patternStop.Stop.TypeStyle == null
                select new
                {
                    patternStop.Stop,
                    RouteCode = route.Code,
                    RouteMode = route.Mode,
                    RouteStoppingPattern = route.StoppingPatternType,
                    RouteOperator = operatorRecord.Code
                }).ToArray();

            Console.WriteLine("Collecting routes servicing each stop");
            
            var stopRouteGrouped = stopRoutesUngrouped.GroupBy(v => v.Stop);

            Console.WriteLine("Finished Collecting");
            
            foreach(var stopRoutes in stopRouteGrouped)
            {
                //Console.WriteLine("Loop Starting");
                var stopRouteArray = stopRoutes.ToArray();
                switch (stopRouteArray.First().RouteMode)
                {
                    case Route.RouteMode.Train:
                        stopRoutes.Key.TypeStyle = StylingType.Train;
                        stopRoutes.Key.FlagStyle = StylingFlag.None;
                        continue;
                    case Route.RouteMode.GLink:
                        stopRoutes.Key.TypeStyle = StylingType.GLink;
                        stopRoutes.Key.FlagStyle = StylingFlag.None;
                        continue;
                    case Route.RouteMode.Ferry when stopRoutes.All(v => CityHopperCodes.Contains(v.RouteCode)):
                        stopRoutes.Key.TypeStyle = StylingType.FerryWhite;
                        stopRoutes.Key.FlagStyle = StylingFlag.FerryCityHopper;
                        continue;
                    case Route.RouteMode.Ferry:
                        stopRoutes.Key.TypeStyle = StylingType.FerryStandard;
                        stopRoutes.Key.FlagStyle = StylingFlag.None;
                        continue;
                }
                //Console.WriteLine("After Switch");
                // I must be a Bus

                if (stopRoutes.Any(v => v.RouteCode[0] == 'R'))
                {
                    stopRoutes.Key.FlagStyle = StylingFlag.RailBus;
                    if (stopRoutes.All(v => v.RouteCode[0] == 'R'))
                    {
                        stopRoutes.Key.TypeStyle = StylingType.BusWhite;
                    }
                }

                
                if (stopRoutes.Any(v => v.RouteOperator == "BT"))
                {
                    if (stopRoutes.Any(v => v.RouteCode == CityGliderBlue))
                    {
                        stopRoutes.Key.FlagStyle = StylingFlag.CityGliderBlue;
                        if (stopRoutes.All(v => v.RouteCode == CityGliderBlue))
                        {
                            stopRoutes.Key.TypeStyle = StylingType.BusWhite;
                        }
                    }
                    else if (stopRoutes.Any(v => v.RouteCode == CityGliderMaroon))
                    {
                        stopRoutes.Key.FlagStyle = StylingFlag.CityGliderMaroon;
                        if (stopRoutes.All(v => v.RouteCode == CityGliderMaroon))
                        {
                            stopRoutes.Key.TypeStyle = StylingType.BusWhite;
                        }
                    }
                    else if (stopRoutes.Any(v => CityLoop.Contains(v.RouteCode)))
                    {
                        stopRoutes.Key.FlagStyle = StylingFlag.BusTheLoop;
                        if (stopRoutes.All(v => CityLoop.Contains(v.RouteCode)))
                        {
                            stopRoutes.Key.TypeStyle = StylingType.BusWhite;
                        }
                    }
                    else if (stopRoutes.Any(v => v.RouteCode == SpringHill))
                    {
                        stopRoutes.Key.FlagStyle = StylingFlag.BusSpringHill;
                        if (stopRoutes.All(v => v.RouteCode == SpringHill))
                        {
                            stopRoutes.Key.TypeStyle = StylingType.BusBlack;
                        }
                    }
                    else if (stopRoutes.Any(v => v.RouteCode == MtCoottha))
                    {
                        stopRoutes.Key.FlagStyle = StylingFlag.BusMtCoottha;
                        if (stopRoutes.All(v => v.RouteCode == MtCoottha))
                        {
                            stopRoutes.Key.TypeStyle = StylingType.BusWhite;
                        }
                    }
                    else if (stopRoutes.Any(v => v.RouteCode == StationLink))
                    {
                        stopRoutes.Key.FlagStyle = StylingFlag.StationLink;
                        if (stopRoutes.All(v => v.RouteCode == StationLink))
                        {
                            stopRoutes.Key.TypeStyle = StylingType.BusWhite;
                        }
                    }

                    if (stopRoutes.Any(v => v.RouteStoppingPattern == Route.RouteStoppingPatternType.CityXpress))
                    {
                        stopRoutes.Key.TypeStyle = StylingType.BccExpress;
                    }
                    else if (stopRoutes.Key.TypeStyle == null)
                    {
                        stopRoutes.Key.TypeStyle = StylingType.BccStandard;
                    }
                }
                // if not TfB
                else if (stopRoutes.Any(v => GcExpressBuses.Contains(v.RouteCode)))
                {
                    stopRoutes.Key.TypeStyle = StylingType.BusWhite;
                    stopRoutes.Key.FlagStyle = StylingFlag.GoldCoastExpress;
                }
                else
                {
                    stopRoutes.Key.TypeStyle = StylingType.BusStandard;
                }

                stopRoutes.Key.FlagStyle ??= StylingFlag.None;
            }
        }
    }
}