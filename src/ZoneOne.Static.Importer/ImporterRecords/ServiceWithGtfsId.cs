using ZoneOne.Static.Data;

namespace ZoneOne.Static.Importer.ImporterRecords
{
    public record ServiceWithGtfsId(string ServiceId, Service Service);
}