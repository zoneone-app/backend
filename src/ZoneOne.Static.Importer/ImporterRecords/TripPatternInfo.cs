using System;
using System.Collections.Generic;

namespace ZoneOne.Static.Importer.ImporterRecords
{
    public record TripPatternInfo(string ShapeId, IEnumerable<PatternTimingInfo> PatternTimings);
    public record GuidTripPatternInfo(Guid ShapeId, IEnumerable<PatternTimingInfo> PatternTimings);
}