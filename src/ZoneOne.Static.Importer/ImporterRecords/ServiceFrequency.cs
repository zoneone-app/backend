using System;

namespace ZoneOne.Static.Importer.ImporterRecords
{
    public record ServiceFrequency(string ServiceId, int Frequency);

    public record GuidServiceFrequency(Guid ServiceId, int Frequency);
}