using System;

namespace ZoneOne.Static.Importer.ImporterRecords
{
    public record HeadwayTripInfo(ServiceFrequency ServiceFrequency, string ShapeId, int FirstDepartureTime, int LastDepartureTime, int Headway);
    public record GuidHeadwayTripInfo(GuidServiceFrequency ServiceFrequency, Guid ShapeId, int FirstDepartureTime, int LastDepartureTime, int Headway);
}