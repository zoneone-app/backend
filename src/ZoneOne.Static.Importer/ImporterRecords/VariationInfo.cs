using ZoneOne.Static.Data;

namespace ZoneOne.Static.Importer.ImporterRecords
{
    public record VariationInfo(StoppingPattern.Variation Start, StoppingPattern.ExtendedVariation Middle,
        StoppingPattern.Variation End);
}