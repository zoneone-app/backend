using System;

namespace ZoneOne.Static.Importer.ImporterRecords
{
    public record StoppingPatternInfo(string ShapeId, bool TripDirection, string Headsign);
    public record GuidStoppingPatternInfo(Guid ShapeId, int Frequency, bool TripDirection, string Headsign);
}