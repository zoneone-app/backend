using System;

namespace ZoneOne.Static.Importer.ImporterRecords
{
    public record TripInfo(ServiceFrequency ServiceFrequency, string ShapeId, int FirstDepartureTime, bool Disabled);
    public record GuidTripInfo(GuidServiceFrequency ServiceFrequency, Guid ShapeId, int FirstDepartureTime, bool Disabled);
}