using System;

namespace ZoneOne.Static.Importer.ImporterRecords
{
    public record ServiceIdMapping(string GtfsId, Guid OurId);
}