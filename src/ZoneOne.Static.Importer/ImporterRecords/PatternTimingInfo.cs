using ZoneOne.Static.Data;

namespace ZoneOne.Static.Importer.ImporterRecords
{
    public record PatternTimingInfo(int TimeToStop, int DwellDuration);
}