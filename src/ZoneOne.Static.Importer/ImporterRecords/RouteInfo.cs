using System.Collections.Generic;
using ZoneOne.Static.Data;

namespace ZoneOne.Static.Importer.ImporterRecords
{
    public record RouteInfo(
        Dictionary<string, TripInfo> TripInfos,
        Dictionary<string, HeadwayTripInfo> HeadwayTripInfos,
        Dictionary<string, int[]> StoplistInfos,
        Dictionary<string, (int[], PatternStop.StoppingType[])> PatternStopsInfos,
        Dictionary<string, TripPatternInfo> TripPatternInfos,
        List<StoppingPatternInfo> StoppingPatternInfos
    );
}