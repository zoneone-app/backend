using System;
using ZoneOne.Static.Data;

namespace ZoneOne.Static.Importer.ImporterRecords
{
    public record OperatorRecord(string Code, Guid Id, bool NewRecord);
}