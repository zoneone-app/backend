using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MoreLinq.Extensions;
using ZoneOne.Static.Importer.Data;
using ZoneOne.Utilities;

namespace ZoneOne.Static.Importer
{
    public partial class StaticDataImporter
    {
        // TODO Move into somewhere out of code
        private static readonly string[] TrainCodes =
        {
            "BD", "VL", "BN", "FG", "IP", "RW",
            "NA", "CA", "GY", "RP", "SP", "CL",
            "SH", "DB", "BR", "EX", "BH", "PR"
        };

        private static readonly string[] CityStationCodes =
        {
            "PR", "BR", "EX", "BH"
        };

        private static readonly string[] ShortInboundTrainCodes =
        {
            "GYCA", "GYSH", "NACA", "NASH", "CASH", "CADB", "RPCA",
            "RPSH", "BDCA", "DBCA", "VLBN", "CLBN", "SPIP", "RWIP"
        };

        private const string MainCityCode = "BR";

        private static bool IsTrainRoute(string code)
        {
            if (code.Length != 4) return false;

            var start = code[..2];
            var dest = code[2..];

            return TrainCodes.Contains(start) && TrainCodes.Contains(dest);
        }

        private static string GetOperatorCode(string serviceId)
        {
            var serviceRegex =
                new Regex("^(?<Operator>[A-Za-z0-9]+)[ _](?<Period>.+)-(?<Version>\\d{2})(?<DOW>-\\d{7})?$");

            return serviceRegex.Match(serviceId).Groups["Operator"].ToString();
        }

        private Task<Guid> GetDbRouteCode(string routeCode, string routeOperator)
        {
            return (from route in _db.Routes.AsQueryable()
                    join operatorRecord in _db.Operators on route.OperatorId equals operatorRecord.Id
                    where route.Code == routeCode && operatorRecord.Code == routeOperator
                    select route.Id).FirstAsync();
        }

        private static string GetTrainCode(string code)
        {
            var start = code[..2];
            var dest = code[2..];

            if (!ShortInboundTrainCodes.Contains(code) && !CityStationCodes.Contains(dest)) return dest;
            if (CityStationCodes.Contains(start)) return MainCityCode;
            return start + MainCityCode;
        }

        private static Static.Data.Route.RouteMode GetRouteMode(Route route) =>
            route.Mode switch
            {
                0 => Static.Data.Route.RouteMode.GLink,
                1 => Static.Data.Route.RouteMode.Train,
                2 => Static.Data.Route.RouteMode.Train,
                4 => Static.Data.Route.RouteMode.Ferry,
                _ => Static.Data.Route.RouteMode.Bus
            };

        private static Static.Data.Route.RouteStyling GetRouteStyle(Static.Data.Route.RouteMode mode) =>
            mode switch
            {
                Static.Data.Route.RouteMode.GLink => Static.Data.Route.RouteStyling.Glink,
                Static.Data.Route.RouteMode.Train => Static.Data.Route.RouteStyling.TrainGrey,
                Static.Data.Route.RouteMode.Ferry => Static.Data.Route.RouteStyling.Ferry,
                _ => Static.Data.Route.RouteStyling.BusStandard
            };

        private async Task ImportRoutes(GtfsLoader gtfs)
        {
            _logger.LogInformation("Importing routes");

            var existingRoutes =
                (from route in _db.Routes.AsQueryable()
                    join operators in _db.Operators on route.OperatorId equals operators.Id
                    select new ValueTuple<string, string>(route.Code, operators.Code)
                ).ToList();

            var routeOperators = gtfs.GetOperator()
                .GroupBy(v => v.RouteId)
                .Select(v => v.FirstAsync())
                .Select(trip => (
                    trip.Result.RouteId,
                    GetOperatorCode(trip.Result.ServiceId)).ToKeyValuePair())
                .ToDictionaryAsync(v => v.Key, v => v.Value).Result;

            await foreach (var route in gtfs.GetRoutes())
            {
                var routeCode = (route.Id.Contains("-")) ? route.Id.SubstringBefore("-") : route.Id;
                var operatorCode = routeOperators[route.Id];
                var operatorId = (await GetDbOperator(operatorCode)).Id;

                // Convert Train Codes
                routeCode = IsTrainRoute(routeCode) ? GetTrainCode(routeCode) : routeCode;

                // skip any routes/operator combination we already have
                if (existingRoutes.Contains((routeCode, operatorCode))) continue;
                existingRoutes.Add((routeCode, operatorCode));

                if (route.Name.Length > 42)
                    _logger.LogWarning("Name of route {Id} is longer than 42 characters, will be truncated", route.Id);

                _logger.LogWarning("Route {Code} (based on {GtfsCode}) is not defined; importing", routeCode, route.Id);

                var mode = GetRouteMode(route);
                var style = GetRouteStyle(mode);
                var alwaysStop = (mode != Static.Data.Route.RouteMode.Bus);

                await _db.Routes.AddAsync(new Static.Data.Route(
                    Guid.NewGuid(),
                    operatorId,
                    routeCode,
                    route.Name[..Math.Min(route.Name.Length, 42)],
                    route.Description, mode, style, alwaysStop
                ));
            }
        }
    }
}