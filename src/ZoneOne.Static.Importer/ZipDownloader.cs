using System;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Messerli.TempDirectory;

namespace ZoneOne.Static.Importer
{
    /// <summary>
    /// Downloads and extracts a zip from the specified path
    /// </summary>
    public class ZipDownloader : IDirectorySource
    {
        private readonly TempDirectory _tempDir;

        public static async Task<ZipDownloader> Download(HttpClient client, string url)
        {
            await using var stream = await client.GetStreamAsync(url);
            if (stream == null) throw new NullReferenceException("Web client stream is null");

            using var archive = new ZipArchive(stream, ZipArchiveMode.Read);

            var tempDir = new TempDirectoryBuilder().Prefix("zip-dl").Create();
            await Task.Run(() => archive.ExtractToDirectory(tempDir.FullName, true));

            return new ZipDownloader(tempDir);
        }

        private ZipDownloader(TempDirectory tempDir)
        {
            _tempDir = tempDir;
        }

        public string Directory => _tempDir.FullName;

        public void Dispose()
        {
            GC.SuppressFinalize(this);
            _tempDir.Dispose();
        }
    }
}