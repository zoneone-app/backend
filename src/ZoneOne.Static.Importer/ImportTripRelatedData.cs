using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using Grassfed.MurmurHash3;
using Microsoft.Extensions.Logging;
using MoreLinq;
using ZoneOne.Static.Data;
using ZoneOne.Static.Data.Util;
using ZoneOne.Static.Importer.Data;
using ZoneOne.Static.Importer.ImporterRecords;
using ZoneOne.Utilities;
using Stop = ZoneOne.Static.Data.Stop;
using Trip = ZoneOne.Static.Importer.Data.Trip;

namespace ZoneOne.Static.Importer
{
    public partial class StaticDataImporter
    {
        private delegate Guid MapShapeId(string id);

        private static readonly int[] SpeedyCatStops =
        {
            319665, 317574, 317572, 317593,
            317590, 317583, 317587, 318002
        };
        
        private (PatternTimingInfo, int) GetPatternTimingInfo(StopTime stopTime, int firstDepartureTime)
        {
            var arrivalTime = (int) DateUtil.ParseAnyHourDate(stopTime.ArrivalTime).TotalMinutes;
            var departureTime = (int) DateUtil.ParseAnyHourDate(stopTime.DepartureTime).TotalMinutes;

            if (firstDepartureTime == -1)
            {
                // this is the first pattern timing
                _logger.LogDebug("Collecting data for trip {TripId}", stopTime.TripId);
                firstDepartureTime = departureTime;
            }

            return (
                new PatternTimingInfo(
                    departureTime - firstDepartureTime,
                    departureTime - arrivalTime
                ),
                firstDepartureTime
            );
        }

        private async Task<Dictionary<(string, string), RouteInfo>> CollectTripRelatedData(
            GtfsLoader gtfs, IDictionary<string, ServiceFrequency> tripFrequencies)
        {
            _logger.LogInformation("Collecting trip related data");
            
            // Group all segments sharing ShapeId
            _logger.LogDebug("Grouping shapes");
            var shapes = await gtfs.GetShapes().ToLookupAsync(s => s.Id);
            _logger.LogDebug("Finish grouping shapes");

            // Collect Initial Trip Data (Trips/Stop_Times csv)
            var tripsAndStopTimes = gtfs.GetStopTimes()
                .GroupBy(v => v.TripId)
                .Zip(gtfs.GetTrips());
            
            var frequencies = await gtfs.GetFrequency().GroupBy(v => v.TripId)
                .ToDictionaryAwaitAsync(v => ValueTask.FromResult(v.Key), v => v.ToArrayAsync());
            
            // Initialise Variables
            var routeInfos = new Dictionary<(string, string), RouteInfo>();
            var shapesList = new List<(string, string)>();
            
            // Group relevant details per trip
            await foreach (var (stopTimesList, trip) in tripsAndStopTimes)
            {
                var stopTimes = await stopTimesList.ToArrayAsync();
                var stopList = new List<int>(stopTimes.Length);
                var stoppingTypes = new List<PatternStop.StoppingType>();
                stopList.AddRange(stopTimes.Select(stopTime => stopTime.StopId));
                stoppingTypes.AddRange(stopTimes.Select(stopTime =>
                    (stopTime.PickupType ? 0 : PatternStop.StoppingType.Pickup) |
                    (stopTime.DropoffType ? 0 : PatternStop.StoppingType.Dropoff)));
                
                var patternTimings = new List<PatternTimingInfo>(stopTimes.Length);
                var (patternTimingInfos, firstDepartureTime) =
                    stopTimes.AggregateSelect(GetPatternTimingInfo, -1);
                patternTimings.AddRange(patternTimingInfos);

                // Handle no path/shape assigned to trip
                if (string.IsNullOrEmpty(trip.ShapeId))
                {
                    _logger.LogWarning("Empty shape ID for trip {TripId}", trip.TripId);
                    trip.ShapeId = $"DUMMY_SHAPE {trip.TripId}";
                }

                // Get Route Details
                var routeOperator = GetOperatorCode(trip.ServiceId);
                var routeCode = (trip.RouteId.Contains("-")) ? trip.RouteId.SubstringBefore("-") : trip.RouteId;

                // Set up Dictionaries if route new
                if (!routeInfos.ContainsKey((routeCode,routeOperator)))
                    routeInfos[(routeCode,routeOperator)] = new RouteInfo(
                        new Dictionary<string, TripInfo>(),
                        new Dictionary<string, HeadwayTripInfo>(),
                        new Dictionary<string, int[]>(),
                        new Dictionary<string, (int[], PatternStop.StoppingType[])>(),
                        new Dictionary<string, TripPatternInfo>(), 
                        new List<StoppingPatternInfo>()
                    );

                var routeInfo = routeInfos[(routeCode,routeOperator)];

                if (frequencies.ContainsKey(trip.TripId))
                {
                    var tripFrequency = frequencies[trip.TripId];
                    
                    foreach (var frequency in tripFrequency)
                    {
                        var startTime = (int) DateUtil.ParseAnyHourDate(frequency.StartTime).TotalMinutes;
                        var endTime = (int) DateUtil.ParseAnyHourDate(frequency.EndTime).TotalMinutes;
                        var freqMin = frequency.Headway / 60;
                        var time = startTime;
                        var counter = 1;
                        while (time < endTime)
                        {
                            // Generate Trips
                            if (frequency.ExactTime == 1)
                            {
                                routeInfo.TripInfos[trip.TripId + "-" + counter] = new TripInfo(
                                    tripFrequencies[trip.ServiceId],
                                    trip.ShapeId, time, false
                                );
                            }
                            else
                            { // Insert into HeadwayTrips instead of Trips
                                if (counter == 1)
                                {
                                    routeInfo.TripInfos[trip.TripId] = new TripInfo(
                                        tripFrequencies[trip.ServiceId],
                                        trip.ShapeId, time, true
                                    );
                                }
                                
                                routeInfo.HeadwayTripInfos[trip.TripId] = new HeadwayTripInfo(
                                    tripFrequencies[trip.ServiceId], trip.ShapeId,
                                    startTime, endTime, freqMin
                                );
                            }
                            time += freqMin;
                            counter++;
                        }
                    }
                }
                else
                {
                    // Store grouping of each trip's details.
                    routeInfo.TripInfos[trip.TripId] = new TripInfo(
                        tripFrequencies[trip.ServiceId],
                        trip.ShapeId, firstDepartureTime, false
                    );
                }
                
                
                
                routeInfo.TripPatternInfos[trip.TripId] = new TripPatternInfo(trip.ShapeId, patternTimings);

                // Only run on new ShapeId
                if (shapesList.Contains((routeOperator, trip.ShapeId))) continue;

                foreach (var stopTime in stopTimes)
                {
                    // Add Only TimingPoints to DB
                    if (!stopTime.TimePoint) continue;
                    var modifiedRouteCode = IsTrainRoute(routeCode) ? GetTrainCode(routeCode) : routeCode;
                    var dbRoute = await GetDbRouteCode(modifiedRouteCode, routeOperator);
                    var dbStop = await GetDbStop(stopTime.StopId);

                    if (_db.TimingPoints.AsQueryable()
                        .Any(v => v.RouteId == dbRoute && v.StopId == dbStop.Id)) continue;
                    
                    await _db.TimingPoints.AddAsync(new TimingPoint(
                        dbRoute,
                        dbStop.Id,
                        true
                    ));
                }
                
                // Collate relevant data for processing
                shapesList.Add((routeOperator, trip.ShapeId));
                routeInfo.StoplistInfos[trip.ShapeId] = stopList.ToArray();
                routeInfo.PatternStopsInfos[trip.ShapeId] = (stopList.ToArray(), stoppingTypes.ToArray());
                routeInfo.StoppingPatternInfos
                    .Add(new StoppingPatternInfo(trip.ShapeId, trip.DirectionId, trip.TripHeadsign));
                try
                {
                    await InsertSegmentStops(shapes, trip, stopList);
                }
                catch
                {
                    _logger.LogWarning("Path is empty for trip {TripId}", trip.TripId);
                }
            }
            return routeInfos;
        }

        private async Task InsertSegmentStops(ILookup<string, Shape> shapes, Trip trip, List<int> stopList)
        {
            var shapeSegment =
                from shape in shapes[trip.ShapeId]
                group shape by Convert.ToInt32(shape.Sequence[..^4])
                into grouping
                select grouping;

            var modifiedShapeSegment = GenerateModifiedShapeSegment(trip, shapeSegment);
            
            _logger.LogDebug("Inserting segment stops");
            
            if (modifiedShapeSegment.Keys.Max() + 1 > stopList.Count - 1)
            {
                _logger.LogWarning("Path has more segments then stops for trip {TripId}", trip.TripId);
                _logger.LogWarning("Segments: {0} StopPairs: {1}",
                    modifiedShapeSegment.Keys.Max() + 1, stopList.Count - 1);
            }
            else if (modifiedShapeSegment.Keys.Max() + 1 < stopList.Count - 1)
            {
                _logger.LogWarning("Path has less segments then stops for trip {TripId}", trip.TripId);
                _logger.LogWarning("Segments: {0} StopPairs: {1}",
                    modifiedShapeSegment.Keys.Max() + 1, stopList.Count - 1);
            }
            else
            {
                var segmentStops = modifiedShapeSegment
                    .Select(segment => new
                    {
                        Stop = _db.Stops.First(v => v.GtfsId == stopList[segment.Key]),
                        NextStop = _db.Stops.First(v => v.GtfsId == stopList[segment.Key + 1]),
                        Coordinates = segment.Value.Select(v => new Coordinate(v.Longitude, v.Latitude))
                    })
                    .DistinctBy(v => (v.Stop.Id, v.NextStop.Id))
                    .Where(v => !_db.SegmentStops.AsQueryable()
                        .Any(x => x.StartStopId == v.Stop.Id && x.EndStopId == v.NextStop.Id))
                    .Select(v => new SegmentStop(
                        v.Stop.Id,
                        v.NextStop.Id,
                        v.Coordinates
                    ));
            
                await _db.SegmentStops.AddRangeAsync(segmentStops);
                await _db.SaveChangesAsync();
                _logger.LogDebug("Done inserting segment stops");
            }
        }

        private Dictionary<int, List<Shape>> GenerateModifiedShapeSegment(
            Trip trip, IEnumerable<IGrouping<int, Shape>> shapeSegment)
        {
            var modifiedShapeSegment = new Dictionary<int, List<Shape>>();
            var offset = 0;

            foreach (var segment in shapeSegment)
            {
                var lastPoint = default(Shape);

                foreach (var point in segment)
                {
                    // Check if point matches last point
                    if (lastPoint != default(Shape) &&
                        Math.Abs(lastPoint.Latitude - point.Latitude) < 0.00005 &&
                        Math.Abs(lastPoint.Longitude - point.Longitude) < 0.00005)
                    {
                        if (segment.Count() == 2)
                        {
                            if (shapeSegment.Count() <= 1)
                            {
                                throw new NoPathException();
                            }
                            // Dummy Segment
                            _logger.LogWarning("Dummy segment for trip {TripId}", trip.TripId);
                            modifiedShapeSegment.Remove(offset);
                            offset--;
                            break;
                        }
                        // if just a redundant point
                        continue;
                    }
                    
                    if (!modifiedShapeSegment.ContainsKey(offset)) modifiedShapeSegment[offset] = new List<Shape>();
                    modifiedShapeSegment[offset].Add(point);

                    lastPoint = point;
                }

                offset++;
            }

            return modifiedShapeSegment;
        }
        
        private static readonly MurmurHash3 Hasher = new MurmurHash3();
        private static byte[] GenerateMurmurHash3(IEnumerable<int> stopList, string routeCode, string routeOperator)
        {
            var data = string.Join('|', stopList);
            var hash = Hasher.ComputeHash(Encoding.UTF8.GetBytes(data));
            var routeBytes = Encoding.UTF8.GetBytes(routeCode.PadLeft(4, ' ') + routeOperator.PadLeft(2,' '));
            return routeBytes.Concat(hash).ToArray()[..16];
        }

        private static Dictionary<string, Guid> GenerateShapeGuidMapping(
            Dictionary<string, int[]> pattern, string routeCode, string routeOperator)
        {
            var list =
                from currentPattern in pattern.ToLookup(v => v.Value.ToEquatable())
                let stopLists = currentPattern.Select(v => v.Value)
                let id = new Guid(GenerateMurmurHash3(stopLists.First(), routeCode, routeOperator))
                from shapeId in currentPattern.Select(v => v.Key)
                select (shapeId, id).ToKeyValuePair();

            return list.ToDictionary(v => v.Key, v => v.Value);
        }

        private static Dictionary<Guid, int> GenerateShapeFrequencies(
            Dictionary<string, TripInfo> trips, MapShapeId mapShapeId)
        {
            var frequenciesByGtfsId = trips.GroupBy(v => v.Value.ShapeId)
                .Select(v => (v.Key, v.Sum(w => w.Value.ServiceFrequency.Frequency)).ToKeyValuePair());

            var frequenciesByGuid = new Dictionary<Guid, int>();

            foreach (var (id, frequency) in frequenciesByGtfsId)
            {
                var guid = mapShapeId(id);
                if (!frequenciesByGuid.ContainsKey(guid)) frequenciesByGuid[guid] = frequency;
                else frequenciesByGuid[guid] += frequency;
            }

            return frequenciesByGuid;
        }

        private static Dictionary<Guid, int[]> MapStoplistsToGuid(Dictionary<string, int[]> stoplist, MapShapeId mapShapeId) =>
            stoplist.DistinctBy(v => mapShapeId(v.Key))
                .Select(v => (mapShapeId(v.Key), v.Value).ToKeyValuePair())
                .ToDictionary();
        
        private static Dictionary<Guid, (int[], PatternStop.StoppingType[])> MapPatternStopsToGuid(Dictionary<string, (int[], PatternStop.StoppingType[])> stoppingType, MapShapeId mapShapeId) =>
            stoppingType.DistinctBy(v => mapShapeId(v.Key))
                .Select(v => (mapShapeId(v.Key), v.Value).ToKeyValuePair())
                .ToDictionary();

        private static IEnumerable<GuidStoppingPatternInfo> MapStoppingPatternsToGuid(
            IEnumerable<StoppingPatternInfo> stoppingPatterns, IReadOnlyDictionary<Guid, int> frequencies, MapShapeId mapShapeId) =>
            from pattern in stoppingPatterns.DistinctBy(v => mapShapeId(v.ShapeId))
            let guid = mapShapeId(pattern.ShapeId)
            select new GuidStoppingPatternInfo(guid, frequencies[guid], pattern.TripDirection, pattern.Headsign);

        private static Dictionary<string, GuidTripPatternInfo> MapPatternTimingsToGuid(
            Dictionary<string, TripPatternInfo> tripPatternInfos, MapShapeId mapShapeId) =>
            tripPatternInfos.Select(v =>(
                v.Key,
                new GuidTripPatternInfo(mapShapeId(v.Value.ShapeId), v.Value.PatternTimings)
            ).ToKeyValuePair()).ToDictionary();

        private static Dictionary<string, GuidTripInfo> MapTripsToGuid(Dictionary<string, TripInfo> trips,
            IReadOnlyDictionary<string, Guid> serviceGuidMapping,
            MapShapeId mapShapeId) =>
            trips.Where(v => v.Value.Disabled == false)
                .Select(v => (
                v.Key,
                new GuidTripInfo(
                    new GuidServiceFrequency(
                        serviceGuidMapping[v.Value.ServiceFrequency.ServiceId],
                        v.Value.ServiceFrequency.Frequency
                    ),
                    mapShapeId(v.Value.ShapeId), v.Value.FirstDepartureTime, v.Value.Disabled)
            ).ToKeyValuePair()).ToDictionary();
        
        private static Dictionary<string, GuidHeadwayTripInfo> MapHeadwayTripsToGuid(Dictionary<string, HeadwayTripInfo> headwayTrips,
            IReadOnlyDictionary<string, Guid> serviceGuidMapping,
            MapShapeId mapShapeId) =>
            headwayTrips.Select(v => (
                v.Key,
                new GuidHeadwayTripInfo(
                    new GuidServiceFrequency(
                        serviceGuidMapping[v.Value.ServiceFrequency.ServiceId],
                        v.Value.ServiceFrequency.Frequency
                    ),
                    mapShapeId(v.Value.ShapeId), v.Value.FirstDepartureTime, v.Value.LastDepartureTime, v.Value.Headway)
            ).ToKeyValuePair()).ToDictionary();

        private static StoppingPattern.Variation CalculateStartVariation(
            IReadOnlyList<int> thisStoplist, IReadOnlyList<int> baseStoplist,
            out IReadOnlyList<int> baseTruncatedStoplist, out IReadOnlyList<int> thisTruncatedStoplist)
        {
            if (thisStoplist.Count == 0)
            {
                baseTruncatedStoplist = baseStoplist;
                thisTruncatedStoplist = thisStoplist;
                return StoppingPattern.Variation.Alternate;
            }
            
            if (thisStoplist[0] != baseStoplist[0])
            {
                // they start at a different place

                if (baseStoplist.Contains(thisStoplist[0])
                    && thisStoplist.Count(v => v == baseStoplist[0]) < baseStoplist.Count(v => v == baseStoplist[0]))
                {
                    // short version
                    baseTruncatedStoplist = baseStoplist.SkipWhile(v => v != thisStoplist[0]).ToArray();
                    thisTruncatedStoplist = thisStoplist;

                    return StoppingPattern.Variation.Short;
                }
                
                if (thisStoplist.Contains(baseStoplist[0]) &&
                         baseStoplist.Count(v => v == thisStoplist[0]) < thisStoplist.Count(v => v == thisStoplist[0]))
                {
                    // extra version
                    
                    // note: extra will appear as alternative if origin stop appears multiple times
                    // (best current workaround)

                    baseTruncatedStoplist = baseStoplist;
                    thisTruncatedStoplist = thisStoplist.SkipWhile(v => v != baseStoplist[0]).ToArray();

                    return StoppingPattern.Variation.Extra;
                }
                
                // alternative version

                var basePatternSet = new HashSet<int>(baseStoplist);

                // make c# happy
                baseTruncatedStoplist = baseStoplist;
                thisTruncatedStoplist = thisStoplist;
    
                foreach (var item in thisStoplist)
                {
                    if (!basePatternSet.Contains(item)) continue;

                    var firstCommonStop = item;
                    baseTruncatedStoplist = baseStoplist.SkipWhile(v => v != firstCommonStop).ToArray();
                    thisTruncatedStoplist = thisStoplist.SkipWhile(v => v != firstCommonStop).ToArray();
                    break;
                }

                return StoppingPattern.Variation.Alternate;
            }

            // set if variables if not matching other conditions
            baseTruncatedStoplist = baseStoplist;
            thisTruncatedStoplist = thisStoplist;
            return StoppingPattern.Variation.Normal;
        }

        private static StoppingPattern.Variation CalculateEndVariation(
            IEnumerable<int> baseTruncatedStoplist, IEnumerable<int> thisTruncatedStoplist,
            out IReadOnlyList<int> baseMiddleStoplist, out IReadOnlyList<int> thisMiddleStoplist)
        {
            var endVariation = CalculateStartVariation(thisTruncatedStoplist.Reverse().ToArray(), 
                baseTruncatedStoplist.Reverse().ToArray(),
                out var baseReversedMiddleStoplist, 
                out var thisReversedMiddleStoplist);

            baseMiddleStoplist = baseReversedMiddleStoplist.Reverse().ToArray();
            thisMiddleStoplist = thisReversedMiddleStoplist.Reverse().ToArray();

            return endVariation;
        }

        private static IEnumerable<int> CompareStoplists(IReadOnlyDictionary<int,int> baseFrequencyAtEachStop,
            IReadOnlyDictionary<int, int> thisFrequencyAtEachStop)
        {
            var usedValues = new List<int>();
            var differences = new Dictionary<int, int>();

            // not sure if differences needs to be a dictionary
            
            foreach (var (key, value) in baseFrequencyAtEachStop)
            {
                if (!usedValues.Contains(key)) usedValues.Add(key);

                if (thisFrequencyAtEachStop.ContainsKey(key)) differences[key] = thisFrequencyAtEachStop[key] - value;
                else differences[key] = -value;
            }

            foreach (var (key, _) in thisFrequencyAtEachStop.Where(kv => !usedValues.Contains(kv.Key)))
            {
                usedValues.Add(key);
                differences[key] = thisFrequencyAtEachStop[key];
            }
            
            return differences.Values;
        }

        private static StoppingPattern.ExtendedVariation CalculateMiddleVariation(IEnumerable<int> thisFullStoplist,
            IEnumerable<int> baseMiddleStoplist, IReadOnlyCollection<int> thisMiddleStoplist)
        {
            // Handle cases where no common stops
            if (thisMiddleStoplist.Count == 0) return StoppingPattern.ExtendedVariation.Alternate;
            
            // Check for Special Stopping Patterns
            // SpeedyCat
            if (thisFullStoplist.All(v => SpeedyCatStops.Contains(v)))
                return StoppingPattern.ExtendedVariation.SpeedyCat;
            
            // Calculate differences between two Stoplists
            // Aka: "crazy magic that only brendan knows how it works"

            var baseFrequencyAtEachStop = baseMiddleStoplist
                .GroupBy(v => v)
                .Select(v => (v.Key, v.Count()).ToKeyValuePair())
                .ToDictionary();

            var thisFrequencyAtEachStop = thisMiddleStoplist
                .GroupBy(v => v)
                .Select(v => (v.Key, v.Count()).ToKeyValuePair())
                .ToDictionary();

            var stopDifferences = CompareStoplists(baseFrequencyAtEachStop, thisFrequencyAtEachStop);
            var (stopsRemoved, stopsAdded) = stopDifferences.CountMany(v => v < 0, v => v > 0);

            // Set Middle Variation
            if (stopsRemoved == 0 && stopsAdded == 0) return StoppingPattern.ExtendedVariation.Normal;
            if (stopsRemoved > 0 && stopsAdded > 0) return StoppingPattern.ExtendedVariation.Alternate;
            if (stopsRemoved > 0) return StoppingPattern.ExtendedVariation.SkipsStops;
            if (stopsAdded > 0) return StoppingPattern.ExtendedVariation.ExtraStops;
            return StoppingPattern.ExtendedVariation.Normal;
        }

        private static VariationInfo CalculateVariation(IReadOnlyList<int> shapePattern, IReadOnlyList<int> basePattern)
        {
            var startVariation = CalculateStartVariation(shapePattern, basePattern,
                out var shortenedBasePattern, 
                out var shortenedShapePattern);

            var endingVariation = CalculateEndVariation(shortenedBasePattern, shortenedShapePattern,
                out var middleBasePattern, 
                out var middleShapePattern);

            var middleVariation = CalculateMiddleVariation(shapePattern, middleBasePattern, middleShapePattern);

            return new VariationInfo(startVariation, middleVariation, endingVariation);
        }

        private static Dictionary<Guid, VariationInfo> GenerateVariations(
            ILookup<bool, IGrouping<bool, GuidStoppingPatternInfo>> shapesByDirection,
            IReadOnlyDictionary<Guid, int[]> patternGuids)
        {
            var result = new Dictionary<Guid, VariationInfo>();

            foreach (var grouping in shapesByDirection)
            {
                var groupingArr = grouping.SelectMany(v => v).ToArray();
                if (groupingArr.Length == 0) continue;

                var mostCommonShape = groupingArr.GetMax(shape => shape.Frequency);
                var basePattern = patternGuids[mostCommonShape.ShapeId].ToArray();

                foreach (var (shapeId, _, _, _) in groupingArr)
                {
                    var shapePattern = patternGuids[shapeId];
                    result[shapeId] = CalculateVariation(shapePattern, basePattern);
                }
            }

            return result;
        }

        private async Task<Stop> GetDbStop(int stopId)
        {
            return await _db.Stops.FirstOrDefaultAsync(v => v.GtfsId == stopId) ??
                   throw new NullReferenceException($"No stop found with Id {stopId}");
        }
        
        private async Task ReformatTripRelatedData(IReadOnlyList<ServiceIdMapping> serviceIdMappings,
            Dictionary<(string, string), RouteInfo> routes)
        {
            // Deal with each route of each operator individually
            foreach (var ((routeCode, routeOperator), info) in routes)
            {
                _logger.LogDebug("Reformat Route: {Route} Operator: {Operator}", routeCode, routeOperator);
                
                // Generate StoppingPatternGUID for each Stopping Pattern
                var shapeGuidMapping = GenerateShapeGuidMapping(info.StoplistInfos, routeCode, routeOperator);
                Guid MapShapeId(string id) => shapeGuidMapping[id];

                // Create GTFSServiceId -> ServiceGUID lookup
                var serviceGuidMapping = serviceIdMappings
                    .ToDictionary(v => v.GtfsId, v => v.OurId);

                // Link StoppingPattern's Stoplist with its GUID 
                _logger.LogTrace("Map StopList with guids");
                var stoplistGuids = MapStoplistsToGuid(info.StoplistInfos, MapShapeId);
                
                // Link StoppingPattern's PatternStops with its GUID 
                _logger.LogTrace("Map PatternStopsInfo with guids");
                var patternStopsGuids = MapPatternStopsToGuid((info.PatternStopsInfos), MapShapeId);
                
                // Link StoppingPattern's Details with its GUID
                _logger.LogTrace("Map stopping pattern details with guids");
                var stoppingPatternGuids = MapStoppingPatternsToGuid(info.StoppingPatternInfos, 
                    GenerateShapeFrequencies(info.TripInfos, MapShapeId), MapShapeId).ToArray();
                
                // Link the PatternTimings with its StoppingPattern GUID
                _logger.LogTrace("Map trip pattern timing with guids");
                var tripPatternTimingGuids = MapPatternTimingsToGuid(info.TripPatternInfos, MapShapeId);
                
                // Link the trip details to the StoppingPattern GUID
                _logger.LogTrace("Map trip guids");
                var tripGuids = MapTripsToGuid(info.TripInfos, serviceGuidMapping, MapShapeId);
                
                // Link the HeadwayTrip details to the StoppingPattern GUID
                var headwayTripGuids = MapHeadwayTripsToGuid(info.HeadwayTripInfos, serviceGuidMapping, MapShapeId);
                
                var (timingProfilesList, tripTimingMap) = (
                    from kvp in tripPatternTimingGuids
                    group kvp by kvp.Value.PatternTimings.ToEquatable() into grouping
                    let guid = Guid.NewGuid()
                    from tripPattern in grouping
                    select ((guid, tripPattern.Value).ToKeyValuePair(), (tripPattern.Key, guid))
                ).Unzip();

                var timingProfiles = timingProfilesList.DistinctBy(v => v.Key).ToDictionary();

                var shapesByDirection = stoppingPatternGuids
                    .GroupBy(v => v.TripDirection)
                    .ToLookup(v => v.Key, v => v);

                var variations = GenerateVariations(shapesByDirection, stoplistGuids);
                var modifiedRouteCode = IsTrainRoute(routeCode) ? GetTrainCode(routeCode) : routeCode;
                var dbRoute = await GetDbRouteCode(modifiedRouteCode, routeOperator);
                
                _db.AddRange(
                    from pattern in stoppingPatternGuids
                    let shapeId = pattern.ShapeId
                    let patternVariations = variations[shapeId]
                    where !_db.StoppingPatterns.AsQueryable().Any(x => x.Id == shapeId)
                    select new StoppingPattern(
                        shapeId,
                        dbRoute,
                        pattern.TripDirection ? StoppingPattern.Dir.B : StoppingPattern.Dir.A,
                        patternVariations.Start,
                        patternVariations.Middle,
                        patternVariations.End
                    ));

                _logger.LogDebug($"Inserting pattern stops for {modifiedRouteCode} operated by {routeOperator}");
                foreach (var (id, (stops,stoppingTypes)) in patternStopsGuids)
                {
                    if (_db.PatternStops.AsQueryable().Any(x => x.StoppingPatternId == id)) continue;
                    for (var i = 0; i < stops.Length; i++)
                    {
                        var stop = await GetDbStop(stops[i]);
                        _db.PatternStops.Add(new PatternStop (
                            id,
                            (short) i,
                            stop.Id,
                            stoppingTypes[i]
                            ));
                    }
                }
                
                _logger.LogDebug($"Inserting pattern timings & timings for {modifiedRouteCode} operated by {routeOperator}");
                foreach (var (id, (shapeId, patternTimingInfos)) in timingProfiles)
                {
                    var patternTimings = patternTimingInfos.ToArray();
                    
                    _db.PatternTimings.Add(new PatternTiming(id, shapeId));

                    for (var i = 0; i < patternTimings.Length; i++)
                    {
                        var (timeToStop, dwellDuration) = patternTimings[i];

                        _db.Timings.Add(new Timing(
                            id,
                            i,
                            (short) timeToStop,
                            (short) dwellDuration
                            ));
                    }
                }
                
                _logger.LogDebug($"Inserting trips for {modifiedRouteCode} operated by {routeOperator}");
                
                _db.Trips.AddRange(tripGuids.Select(v => new Static.Data.Trip(
                    Guid.NewGuid(),
                    v.Key,
                    v.Value.ServiceFrequency.ServiceId,
                    tripTimingMap.First(el => v.Key.StartsWith(el.Key)).guid,
                    v.Value.ShapeId,
                    (short) v.Value.FirstDepartureTime
                    )));
                
                _db.HeadwayTrips.AddRange(headwayTripGuids.Select(v => new Static.Data.HeadwayTrip(
                    Guid.NewGuid(),
                    v.Key,
                    v.Value.ServiceFrequency.ServiceId,
                    tripTimingMap.First(el => el.Key == v.Key).guid,
                    v.Value.ShapeId,
                    (short) v.Value.FirstDepartureTime,
                    (short) v.Value.LastDepartureTime,
                    (short) v.Value.Headway
                )));
            }
        }

        private async Task ImportTripRelatedData(GtfsLoader gtfs, IEnumerable<ServiceIdMapping> serviceIdMappings,
            IDictionary<string, ServiceFrequency> tripFrequencies)
        {
            _logger.LogInformation("Importing trip related data");
            var tripDict = await CollectTripRelatedData(gtfs, tripFrequencies);
            _logger.LogInformation("Reformatting trip related data");
            await ReformatTripRelatedData(serviceIdMappings.ToArray(), tripDict);
        }
    }
}