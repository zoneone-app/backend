using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Reflection;
using CsvHelper;
using CsvHelper.Configuration;
using ZoneOne.Static.Importer.Data;
using Calendar = ZoneOne.Static.Importer.Data.Calendar;

namespace ZoneOne.Static.Importer
{
    public sealed class GtfsLoader : IDisposable
    {
        private readonly IDirectorySource _source;

        private readonly CsvReader _calendarReader;
        private readonly CsvReader _calendarExceptionReader;
        private readonly CsvReader _routeReader;
        private readonly CsvReader _shapeReader;
        private readonly CsvReader _stopReader;
        private readonly CsvReader _stopTimeReader;
        private readonly CsvReader _tripReader;
        private readonly CsvReader _operatorReader;
        private readonly CsvReader _frequenciesReader;

        private CsvReader GetCsvReader(string src)
        {
            var path = Path.Join(_source.Directory, src);
            var reader = new StreamReader(path);
            var config = new CsvConfiguration(CultureInfo.InvariantCulture)
            {
                HeaderValidated = null,
                MissingFieldFound = null
            };
            //return new CsvReader(reader, CultureInfo.InvariantCulture);
            return new CsvReader(reader, config);
        }

        private void CreateDummyFiles(string src)
        {
            if (!File.Exists(Path.Join(_source.Directory, src)))
            {
                File.Create(Path.Join(_source.Directory, src)).Close();
            }
        }
        
        public GtfsLoader(IDirectorySource source)
        {
            _source = source;
            
            // Optional Files
            CreateDummyFiles("calendar_dates.txt");
            _calendarExceptionReader = GetCsvReader("calendar_dates.txt");
            
            CreateDummyFiles("frequencies.txt");
            _frequenciesReader = GetCsvReader("frequencies.txt");
            
            // Required Files
            _calendarReader = GetCsvReader("calendar.txt");
            _routeReader = GetCsvReader("routes.txt");
            _shapeReader = GetCsvReader("shapes.txt");
            _stopReader = GetCsvReader("stops.txt");
            _stopTimeReader = GetCsvReader("stop_times.txt");
            _tripReader = GetCsvReader("trips.txt");
            
            // Generate extra copy
            File.Copy(Path.Join(_source.Directory, "trips.txt"),Path.Join(_source.Directory, "trips_operators.txt"));
            _operatorReader = GetCsvReader("trips_operators.txt");
        }

        public IAsyncEnumerable<Calendar> GetCalendars() => _calendarReader.GetRecordsAsync<Calendar>();
        public IAsyncEnumerable<CalendarException> GetCalendarExceptions() => _calendarExceptionReader.GetRecordsAsync<CalendarException>();
        public IAsyncEnumerable<Route> GetRoutes() => _routeReader.GetRecordsAsync<Route>();
        public IAsyncEnumerable<Shape> GetShapes() => _shapeReader.GetRecordsAsync<Shape>();
        public IAsyncEnumerable<Stop> GetStops() => _stopReader.GetRecordsAsync<Stop>();
        public IAsyncEnumerable<StopTime> GetStopTimes() => _stopTimeReader.GetRecordsAsync<StopTime>();
        public IAsyncEnumerable<Trip> GetTrips() => _tripReader.GetRecordsAsync<Trip>();
        public IAsyncEnumerable<Trip> GetOperator() => _operatorReader.GetRecordsAsync<Trip>();
        public IAsyncEnumerable<Frequency> GetFrequency() => _frequenciesReader.GetRecordsAsync<Frequency>();

        public void Dispose()
        {
            _calendarReader.Dispose();
            _calendarExceptionReader.Dispose();
            _routeReader.Dispose();
            _shapeReader.Dispose();
            _stopReader.Dispose();
            _stopTimeReader.Dispose();
            _tripReader.Dispose();
            _operatorReader.Dispose();
            _frequenciesReader.Dispose();
        }
    }
}