using System.Threading.Tasks;

namespace ZoneOne.Static.Importer
{
    public interface IStaticDataImporter
    {
        public Task RunAsync();
    }
}