﻿using System;
using System.Runtime.Serialization;

namespace ZoneOne.Static.Importer
{
    [Serializable]
    public class NoPathException : Exception
    {
        //
        // For guidelines regarding the creation of new exception types, see
        //    http://msdn.microsoft.com/library/default.asp?url=/library/en-us/cpgenref/html/cpconerrorraisinghandlingguidelines.asp
        // and
        //    http://msdn.microsoft.com/library/default.asp?url=/library/en-us/dncscol/html/csharp07192001.asp
        //

        public NoPathException()
        {
        }

        public NoPathException(string message) : base(message)
        {
        }

        public NoPathException(string message, Exception inner) : base(message, inner)
        {
        }

        protected NoPathException(
            SerializationInfo info,
            StreamingContext context) : base(info, context)
        {
        }
    }
}