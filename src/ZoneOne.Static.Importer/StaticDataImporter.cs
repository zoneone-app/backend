﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using StackExchange.Redis;
using ZoneOne.Static.Database;
using ZoneOne.Utilities;

namespace ZoneOne.Static.Importer
{
    public partial class StaticDataImporter : IStaticDataImporter
    {
        private readonly ILogger<StaticDataImporter> _logger;
        private readonly IConfiguration _configuration;
        private readonly StaticDbContext _db;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly IDatabase _redisCache;

        public StaticDataImporter(
            ILogger<StaticDataImporter> logger,
            IConfiguration configuration,
            StaticDbContext db, IHttpClientFactory httpClientFactory,
            IDatabase redisCache
        )
        {
            _logger = logger;
            _configuration = configuration;
            _db = db;
            _httpClientFactory = httpClientFactory;
            _redisCache = redisCache;
        }
        
        public async Task RunAsync()
        {
            AppDomain domain = AppDomain.CurrentDomain;
            domain.AssemblyResolve += (_, args) =>
            {
                Console.WriteLine($"Attempting to resolve {args.Name} (Requested by {args.RequestingAssembly}");
                return null;
            };
            // Get Dataset Urls
            var sourceUrls = _configuration
                .GetSection("Importer")
                .GetSection("Source").Get<string[]>();
            
            // Test Data

            foreach (var sourceUrl in sourceUrls)
            {
                if (!await RemoteFileExists.UrlIsValidAsync(sourceUrl))
                {
                    throw new InvalidUrlException();
                }
            }
            
            // Clear Temp Tables
            _logger.LogDebug("Emptying tables");
            await _db.Database
                .ExecuteSqlRawAsync(@"TRUNCATE TABLE ""Trips"", ""HeadwayTrips"", ""Timings"", ""PatternTimings"", ""ServiceExceptions"", ""Services""");
            
            // Get Data
            foreach (var sourceUrl in sourceUrls)
            {
                _logger.LogInformation("Downloading GTFS zip from {SourceUrl}", sourceUrl);
                using var downloaderClient = _httpClientFactory.CreateClient("importer");
                using var downloader = await ZipDownloader.Download(downloaderClient, sourceUrl);
                _logger.LogDebug("Download complete");
            
                using var gtfs = new GtfsLoader(downloader);
                
                var (serviceMapping, tripFrequencies) = await ImportServicesAndOperators(gtfs);
                await ImportStops(gtfs);
                await ImportRoutes(gtfs);
                await _db.SaveChangesAsync();
                await ImportTripRelatedData(gtfs, serviceMapping, tripFrequencies);
                await _db.SaveChangesAsync();
                await StyleStops();
                await _db.SaveChangesAsync();
                _logger.LogInformation("Finished Importing GTFS zip from {SourceUrl}", sourceUrl);
            }
            _logger.LogInformation("Finished Importing GTFS");
        }
    }
}