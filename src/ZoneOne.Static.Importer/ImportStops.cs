using System;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using ZoneOne.Static.Data;
using ZoneOne.Static.Data.Util;
using ZoneOne.Utilities;

namespace ZoneOne.Static.Importer
{
    public partial class StaticDataImporter
    {
        private static readonly Regex TitleRegex = new("<title>[\\w\\d',() -]+, (?<Name>[\\w -]+)(?: \\([\\w ]+\\))? \\| TransLink<\\/title>");

        private async Task<string?> GetSuburb(Data.Stop stop)
        {
            _logger.LogTrace("Getting suburb for stop {StopId}", stop.Id);

            var redisKey = $"importer.suburb.{stop.Id.PadLeft(6, '0')}";

            if (await _redisCache.KeyExistsAsync(redisKey))
            {
                _logger.LogTrace("Value exists in cache");
                var value = await _redisCache.StringGetAsync(redisKey);
                if (!string.IsNullOrEmpty(value)) return value;
            }

            try
            {
                using var client = _httpClientFactory.CreateClient("importer");
                var pageContent =
                    await client.GetStringAsync($"https://jp.translink.com.au/plan-your-journey/stops/{stop.Id}");

                // not a very good way to do this but it works
                var suburb = TitleRegex.Match(pageContent).Groups["Name"].Value;
                await _redisCache.StringSetAsync(redisKey, suburb, TimeSpan.FromDays(28));

                _logger.LogDebug("Stop {StopId} is in {Suburb}", stop.Id, suburb);

                return suburb;
            }
            catch (HttpRequestException ex)
            {
                _logger.LogError(ex, "Translink website error at {StopId}", stop.Id);
                return null;
            }
        }
        
        private async Task ImportStops(GtfsLoader gtfs)
        {
            _logger.LogInformation("Importing stops");
            
            var stops = gtfs.GetStops()
                .Where(v => v.Type == 0)
                .Where(x => !_db.Stops.AsQueryable().Any(y => y.GtfsId.ToString() == x.Id))
                .SelectAwait(async stop => new Stop
                    (Guid.NewGuid(),
                    Convert.ToInt32(stop.Id),
                    stop.Name,
                    stop.Longitude,
                    stop.Latitude,
                    string.IsNullOrEmpty(stop.Url) ? null : await GetSuburb(stop),
                    (Zone) Convert.ToByte(stop.Zone.SubstringBefore("/").DefaultTo("0")),
                    string.IsNullOrEmpty(stop.Url)
                    )
                {
                    OtherZone = stop.Zone.Contains('/') ? (Zone?) Convert.ToByte(stop.Zone.SubstringAfter("/")) : null
                });

            await _db.Stops.AddRangeAsync(stops.ToEnumerable());
        }
    }
}