using System;

namespace ZoneOne.Static.Data
{
    public record RoutePair(Guid AId, Guid BId, string Name) : DatedEntity
    {
        public virtual Route A { get; set; }
        public virtual Route B { get; set; }
    }
}