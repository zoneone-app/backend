using System;
using System.Collections.Generic;
using ZoneOne.Static.Data.Util;

namespace ZoneOne.Static.Data
{
    public record Stop(
        Guid Id, int GtfsId, string Name, float Longitude, float Latitude, string? Suburb, Zone Zone,
        bool IsDummy) : DatedEntity
    {
        public string? ChildLabel { get; set; }
        public string? ChildLabelDescription { get; set; }
        public Zone? OtherZone { get; set; }
        public StylingType? TypeStyle { get; set; }
        public StylingFlag? FlagStyle { get; set; }
        public bool? HideId { get; set; }

        public virtual Stop? MergeIntoStop { get; init; }
        public virtual IEnumerable<Stop>? MergedStops { get; set; }
        public virtual Station? Station { get; set; }
        public virtual IEnumerable<PatternStop>? PatternStops { get; set; }
        public virtual IEnumerable<SegmentStop>? StartSegmentStops { get; set; }
        public virtual IEnumerable<SegmentStop>? EndSegmentStops { get; set; }
        public virtual IEnumerable<TimingPoint>? TimingPoints { get; set; }
        public virtual IEnumerable<ReplacementStop>? ReplacementStop { get; init; }
        public virtual IEnumerable<PatternStopOverrides>? ExistingPatternStops { get; set; }
        public virtual IEnumerable<PatternStopOverrides>? NewPatternStops { get; set; }
        public virtual IEnumerable<ContinuingRoute>? ContinuingRoutes { get; set; }
        
        public virtual IEnumerable<StationStopsRelation>? StopPairA { get; set; }
        public virtual IEnumerable<StationStopsRelation>? StopPairB { get; set; }
    }
}