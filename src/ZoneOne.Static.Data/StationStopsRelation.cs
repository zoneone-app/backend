using System;
using System.Collections.Generic;

namespace ZoneOne.Static.Data
{
    public record StationStopsRelation(Guid StationId, Guid StopAId, Guid StopBId, StationStopsRelation.StationStopRelations Relation) : DatedEntity
    {
        public enum StationStopRelations
        {
            Island
        }
        
        public virtual Station Station { get; init; }
        public virtual Stop StopA { get; init; }
        public virtual Stop StopB { get; init; }
    }
}