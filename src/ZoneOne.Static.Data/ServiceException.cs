using System;

namespace ZoneOne.Static.Data
{
    public record ServiceException(Guid ServiceId, int Date, bool Type)
    {
        public virtual Service Service { get; set; }
    }
}