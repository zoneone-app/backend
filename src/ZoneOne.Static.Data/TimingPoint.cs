
using System;
using System.Collections.Generic;

namespace ZoneOne.Static.Data
{
    public record TimingPoint(Guid RouteId, Guid StopId, bool IsTimingPoint) : DatedEntity
    {
        public virtual Route Route { get; set; }
        public virtual Stop Stop { get; set; }
    }
}