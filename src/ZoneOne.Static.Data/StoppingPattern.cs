using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ZoneOne.Static.Data
{
    public record StoppingPattern(
        Guid Id, Guid RouteId, StoppingPattern.Dir Direction,
        StoppingPattern.Variation StartVariation,
        StoppingPattern.ExtendedVariation MiddleVariation,
        StoppingPattern.Variation EndVariation
        ) : DatedEntity
    {
        public enum ExtendedVariation
        {
            Normal,
            SpeedyCat,
            SkipsStops,
            ExtraStops,
            Alternate
        }

        public enum Variation
        {
            Normal,
            Short,
            Extra,
            Alternate
        }

        public enum Dir
        {
            A,
            B,
            C
        }
        
        public string? StoppingPatternLabel { get; set; }
        public string? MiddleVariationLabel { get; set; }

        public virtual Route Route { get; set; }
        public virtual IEnumerable<PatternStop>? PatternStops { get; set; }
        public virtual IEnumerable<PatternTiming>? PatternTimings { get; set; }
        public virtual IEnumerable<Trip>? Trips { get; set; }
        public virtual IEnumerable<HeadwayTrip>? HeadwayTrips { get; set; }
    }
}