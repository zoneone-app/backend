using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using ZoneOne.Static.Data.Util;

namespace ZoneOne.Static.Data
{
    public record Service(Guid Id, Guid OperatorId, Days Days, int Start, int End, string Period, int Version)
    {
        public virtual Operator Operator { get; set; }
        public virtual IEnumerable<ServiceException>? Exceptions { get; set; }
        public virtual IEnumerable<Trip>? Trips { get; set; }
        public virtual IEnumerable<HeadwayTrip>? HeadwayTrips { get; set; }
    }
}