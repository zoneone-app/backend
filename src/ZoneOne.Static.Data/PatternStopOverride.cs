
using System;
using System.Collections.Generic;

namespace ZoneOne.Static.Data
{
    public record PatternStopOverrides(Guid RouteId, Guid ExistingStopId, Guid NewStopId) : DatedEntity
    {
        public virtual Route Route { get; set; }
        public virtual Stop ExistingStop { get; set; }
        public virtual Stop NewStop { get; set; }
    }
}