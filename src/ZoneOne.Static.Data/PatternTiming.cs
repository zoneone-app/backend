using System;
using System.Collections.Generic;

namespace ZoneOne.Static.Data
{
    public record PatternTiming(Guid Id, Guid StoppingPatternId)
    {
        public virtual StoppingPattern StoppingPattern { get; set; }
        public virtual IEnumerable<Trip>? Trips { get; set; }
        public virtual IEnumerable<HeadwayTrip>? HeadwayTrips { get; set; }
        public virtual IEnumerable<Timing>? Timings { get; set; }
    }
}