using System;
using System.Collections.Generic;
using System.Linq;
using ZoneOne.Static.Data.Util;

namespace ZoneOne.Static.Data
{
    public class SegmentStop : DatedEntityClass
    {
        public SegmentStop(
            Guid startStopId,
            Guid endStopId,
            IEnumerable<Coordinate> path)
        {
            StartStopId = startStopId;
            EndStopId = endStopId;
            Path = path;
        }

        public SegmentStop(
            Guid startStopId,
            Guid endStopId,
            string pathStr)
        {
            StartStopId = startStopId;
            EndStopId = endStopId;
            PathStr = pathStr;
        }

        private static IEnumerable<Coordinate> GetPath(string source) =>
            source.Split('|')
                .Select(v => v.Split(':'))
                .Select(v => new Coordinate(
                    Convert.ToSingle(v[0]),
                    Convert.ToSingle(v[1])
                ));

        private static string SetPath(IEnumerable<Coordinate> source) =>
            string.Join('|', source
                .Select(v => $"{v.Longitude}:{v.Latitude}")
            );

        public string PathStr { get; set; }
        public string? PathStr2 { get; set; }
        public string? PathStr3 { get; set; }

        public IEnumerable<Coordinate> Path
        {
            get => GetPath(PathStr);
            set => PathStr = SetPath(value);
        }

        public IEnumerable<Coordinate> Path2
        {
            get => GetPath(PathStr2 ?? "");
            set => PathStr2 = SetPath(value);
        }

        public IEnumerable<Coordinate> Path3
        {
            get => GetPath(PathStr3 ?? "");
            set => PathStr3 = SetPath(value);
        }

        public virtual Stop StartStop { get; set; }
        public virtual Stop EndStop { get; set; }
        public Guid StartStopId { get; init; }
        public Guid EndStopId { get; init; }

        public void Deconstruct(out Guid StartStopId, out Guid EndStopId)
        {
            StartStopId = this.StartStopId;
            EndStopId = this.EndStopId;
        }
    }
}