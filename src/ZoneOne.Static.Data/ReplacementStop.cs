using System;
using System.Collections.Generic;

namespace ZoneOne.Static.Data
{
    public record ReplacementStop(Guid StationId, Guid StopId, string Label) : DatedEntity
    {
        public virtual Station Station { get; init; }
        public virtual Stop Stop { get; init; }
    }
}