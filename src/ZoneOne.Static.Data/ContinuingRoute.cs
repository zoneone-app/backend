using System;
using System.Collections.Generic;

namespace ZoneOne.Static.Data
{
    public record ContinuingRoute(Guid RouteInitialId, Guid RouteFinalId, Guid StopId) : DatedEntity
    {
        public virtual Route RouteInitial { get; set; }
        public virtual Route RouteFinal { get; set; }
        public virtual Stop Stop { get; set; }
    }
}