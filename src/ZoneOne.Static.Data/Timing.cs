using System;
using System.Collections.Generic;

namespace ZoneOne.Static.Data
{
    public record Timing(
        Guid PatternTimingId,
        int PositionInSequence, short TimeToStop, short DwellDuration
        )
    {
        public virtual PatternTiming PatternTiming { get; set; }
    }
}