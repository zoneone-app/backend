using System;
using System.Collections.Generic;

namespace ZoneOne.Static.Data
{
    public record Route(
        Guid Id,
        Guid OperatorId,
        string Code,
        string Title,
        string Description,
        Route.RouteMode Mode,
        Route.RouteStyling Styling,
        bool AlwaysStop
    ) : DatedEntity
    {
        public enum RouteDirection
        {
            TwoWay,
            InvTwoWay,
            Primary,
            Secondary,
            Segments
        }

        public enum RouteDirectionType
        {
            InOut,
            NorthSouth,
            EastWest,
            Clock,
            Stream
        }

        public enum RouteMode
        {
            Bus,
            Train,
            Citycat,
            CityHopper,
            CityFerry,
            Ferry,
            GLink,
            RailBus
        }

        public enum RouteStoppingPatternType
        {
            AllStops,
            Local,
            LocalExpress,
            LimitedStops,
            CityXpress,
            Busway,
            LimitedExpress,
            Express
        }

        public enum RouteBranding
        {
            None,
            Buz,
            CityGlider,
            Rocket,
            CityPrecincts,
            Bullet,
            NightLink,
            StationLink
        }

        public enum RouteStyling
        {
            BusExpress,
            BusStandard,
            CityGliderBlue,
            CityGliderMaroon,
            CityHopper,
            CityLoop,
            Ferry,
            Glink,
            GoldCoastExpress,
            MtCoottha,
            NightLink,
            SpringHill,
            TfbBuz,
            TfbExpress,
            TfbStandard,
            Train,
            TrainDarkBlue,
            TrainGreen,
            TrainGrey,
            TrainLightBlue,
            TrainPurple,
            TrainRed,
            TrainYellow,
            StationLink,
            GUShuttle,
            BAC,
            QUT
        }

        public string? ShortTitle { get; set; }
        public RouteDirectionType? DirectionType { get; set; }
        public RouteDirection? Direction { get; set; }
        public RouteBranding? Branding { get; set; }
        public RouteStoppingPatternType? StoppingPatternType { get; set; }
        public bool? HailRide { get; set; }
        public bool? Free { get; set; }
        public bool? School { get; set; }

        public virtual Operator? Operator { get; set; }
        public virtual IEnumerable<StoppingPattern>? StoppingPatterns { get; set; }
        public virtual RoutePair? PairA { get; set; }
        public virtual RoutePair? PairB { get; set; }
        public virtual IEnumerable<TimingPoint>? TimingPoints { get; set; }
        public virtual IEnumerable<PatternStopOverrides>? PatternStopOverrides { get; set; }
        public virtual IEnumerable<ContinuingRoute>? RouteInitials { get; set; }
        public virtual IEnumerable<ContinuingRoute>? RouteFinals { get; set; }
    }
}