using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using ZoneOne.Static.Data.Util;

namespace ZoneOne.Static.Data
{
    public record Operator(Guid Id, string Code) : DatedEntity
    {
        public string? Label { get; set; }
        public string? Prefix { get; set; }
        
        public virtual IEnumerable<Route>? Route { get; set; }
        public virtual IEnumerable<Service>? Service { get; set; }
    }
}