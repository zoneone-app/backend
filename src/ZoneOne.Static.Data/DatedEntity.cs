﻿using System;

namespace ZoneOne.Static.Data
{
    public interface IDatedEntity
    {
        public DateTime Created { get; set; }
        public DateTime LastUpdated { get; set; }
    }
    public abstract record DatedEntity : IDatedEntity
    {
        public DateTime Created { get; set; }
        public DateTime LastUpdated { get; set; }
    }
    
    public abstract class DatedEntityClass : IDatedEntity
    {
        public DateTime Created { get; set; }
        public DateTime LastUpdated { get; set; }
    }
}