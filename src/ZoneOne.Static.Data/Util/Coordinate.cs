namespace ZoneOne.Static.Data.Util
{
    public record Coordinate(float Longitude, float Latitude);
}