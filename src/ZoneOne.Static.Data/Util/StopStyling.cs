namespace ZoneOne.Static.Data.Util
{
    public enum StylingType
    {
        BccExpress,
        BccStandard,
        BusWhite,
        BusBlack,
        BusStandard,
        BusExpress,
        FerryStandard,
        FerryWhite,
        GLink,
        Train,
        BusStation,
        GLinkWhite,
        GLinkBus,
        GroupOrange,
        GroupGreen,
        BccMinor
    }
    
    public enum StylingFlag
    {
        None,
        CityGliderBlue,
        CityGliderMaroon,
        BusTheLoop,
        BusSpringHill,
        BusNightLink,
        BusMtCoottha,
        RailBus,
        BusGcExpress,
        GoldCoastExpress,
        FerryCityHopper,
        Busway,
        StationLink,
        GUShuttle,
        BAC,
        QUT
    }
}