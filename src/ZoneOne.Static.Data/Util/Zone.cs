namespace ZoneOne.Static.Data.Util
{
    public enum Zone
    {
        Airport,
        Zone1,
        Zone2,
        Zone3,
        Zone4,
        Zone5,
        Zone6,
        Zone7,
        Zone8
    }
}