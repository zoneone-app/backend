using System;
using System.Collections.Generic;
using ZoneOne.Static.Data.Util;

namespace ZoneOne.Static.Data
{
    public record Station(Guid Id, string Name, string TypeLabel, float Longitude, float Latitude,
        Station.LogicType StationType, StylingType TypeStyle, StylingFlag FlagStyle) : DatedEntity
    {
        public enum LogicType
        {
            Interchange,
            KIP,
            Bayed,
            Platformed,
            GLink,
            Train,
            Directional,
            UIGrouping
        }
        
        public string? Desto { get; set; }
        public string? ChildLabel { get; set; }
        public string? ChildLabelDescription { get; set; }
        public virtual IEnumerable<Station>? ChildrenStations { get; init; }
        public virtual IEnumerable<Stop>? ChildrenStops { get; init; }
        public virtual IEnumerable<ReplacementStop>? ReplacementStop { get; init; }
        public virtual IEnumerable<StationStopsRelation>? StopRelationPair { get; init; }
        public virtual Station? StationGroup { get; init; }
    }
}