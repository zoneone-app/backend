using System;
using System.Collections.Generic;

namespace ZoneOne.Static.Data
{
    public record PatternStop(Guid StoppingPatternId, short PositionInSequence, Guid StopId, PatternStop.StoppingType StopType)
    {
        [Flags]
        public enum StoppingType
        {
            Pickup = 0b01,
            Dropoff = 0b10
        }
        
        public bool? NewDestination { get; set; }
        public bool? ContinuousDestination { get; set; }
        public virtual StoppingPattern StoppingPattern { get; set; }
        public virtual Stop Stop { get; set; }
    }
}