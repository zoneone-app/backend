using System;

namespace ZoneOne.Static.Data
{
    public record HeadwayTrip(
        Guid Id,
        string GtfsTripId,
        Guid ServiceId,
        Guid PatternTimingId,
        Guid StoppingPatternId,
        short StartTime,
        short EndTime,
        short Headway
        )
    {
        public virtual Service Service { get; set; }
        public virtual PatternTiming PatternTiming { get; set; }
        public virtual StoppingPattern StoppingPattern { get; set; }
    }
}