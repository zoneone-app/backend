using System;

namespace ZoneOne.Static.Data
{
    public record Trip(
        Guid Id,
        string GtfsTripId,
        Guid ServiceId,
        Guid PatternTimingId,
        Guid StoppingPatternId,
        short StartTime
        )
    {
        public virtual Service Service { get; set; }
        public virtual PatternTiming PatternTiming { get; set; }
        public virtual StoppingPattern StoppingPattern { get; set; }
    }
}